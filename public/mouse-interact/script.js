spos=document.getElementsByClassName('resize-drag')
sposx=100
for ( i=0;i<spos.length;i++){spos[i].setAttribute('data-x',sposx);}
document.getElementById('p3').setAttribute('data-x',sposx)
document.getElementById('floor').setAttribute('data-x',277)
	
function imgToggle(id) {
  var x = document.getElementById(id);
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
} 

var dupimgCount=1
function dupimg(){
	var base = document.createElement('img');
	var imgdiv= document.createElement('div');
	base.className = "draggable"; base.id='p3';
	base.src='pic/socket2.png';
	base.setAttribute('data-x',sposx);
	base.style.zIndex=dupimgCount;
	dupimgCount++;
	/*
	imgdiv.className = "resize-container";
	imgdiv.id='divp3'
	imgdiv.appendChild(base);
	*/
	var targetelement = document.querySelector('#add');
	targetelement.parentNode.insertBefore(base, targetelement.nextSibling);
	//targetelement.parentNode.insertBefore(imgdiv, targetelement.nextSibling);
	//<div class="resize-container"><img src='pic/socket2.png' class="draggable" id=p3></div>
}
window.PointerEvent = window.MSPointerEvent = null;
interact('.draggable').draggable({
    onmove: window.dragMoveListener
  })
interact('.resize-drag')
  .draggable({
    onmove: window.dragMoveListener
  })
  .resizable({
    preserveAspectRatio: false,
    edges: { left: true, right: true, bottom: true, top: true },   
    invert: 'reposition'
  })
  .on('resizemove', function (event) {
    var target = event.target,
        x = (parseFloat(target.getAttribute('data-x')) || 0),
        y = (parseFloat(target.getAttribute('data-y')) || 0);

    // update the element's style
    target.style.width  = event.rect.width + 'px';
    target.style.height = event.rect.height + 'px';

    // translate when resizing from top or left edges
    x += event.deltaRect.left;
    y += event.deltaRect.top;
    
    target.style.webkitTransform = target.style.transform =
        'translate(' + x + 'px,' + y + 'px)';

    target.setAttribute('data-x', x);
    target.setAttribute('data-y', y);
    //target.textContent = event.rect.width + '×' + event.rect.height;
  });

function dragMoveListener (event) {
    var target = event.target,
        // keep the dragged position in the data-x/data-y attributes
        x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
        y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

    // translate the element
    target.style.webkitTransform =
    target.style.transform =
      'translate(' + x + 'px, ' + y + 'px)';

    // update the posiion attributes
    target.setAttribute('data-x', x);
    target.setAttribute('data-y', y);
  }
