import pandas as pd
import numpy as np

df=pd.read_csv('mydata.csv',encoding='UTF-8')
#df = df.drop('index', axis=1)

from sklearn.preprocessing import LabelEncoder
le = LabelEncoder()

#水體等級 water_quailty
wq='水體分類等級'
print(df[df.columns[1]].unique())

cName = df.columns.tolist()
features = cName[2:]
X = pd.DataFrame(df[features])
y = le.fit_transform(df[wq])

from sklearn.model_selection import train_test_split as tts
from sklearn import tree, cluster
from sklearn.cluster import DBSCAN
from sklearn.metrics import silhouette_score as ss
from sklearn import metrics
import matplotlib as mpl
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import NearestNeighbors
from kneed import KneeLocator
import warnings

#cad='鎘' #df.columns[13]
wtmp='水溫_度C'
wq_rating=['丁','丙','乙','戊',]

#remove 丁
def removeD():
    df[wq].replace('丁', np.nan, inplace=True)
    df.dropna(axis=0, subset=[wq], inplace=True )
    wq_rating=['丙','乙','戊',]
    print(df.groupby(wq).size())

for index, value in enumerate(cName):
   print(index, value)

def zhfontsetting(p):
    #colab use
    if p == 'sns':
        plt.style.use('seaborn-v0_8')
    elif p == 'plt':
        mpl.rcParams.update(mpl.rcParamsDefault)
    #plt.rcParams['axes.unicode_minus'] = False
    fontManager.addfont('TaipeiSansTCBeta-Regular.ttf')
    mpl.rc('font', family='Taipei Sans TC Beta')

#dbscan functions
#mpl.rcParams.update(mpl.rcParamsDefault)
def cName2(dc1,dc2):
  print(cName[dc1], cName[dc2])

def dbs(c1, c2, ev, msv):
  X2 = df.iloc[:,[c1,c2]].to_numpy()

  scalar=StandardScaler()
  scalar.fit(X2)
  X2_std=scalar.transform(X2)

  clustering = DBSCAN(eps=ev, min_samples=msv).fit(X2_std)
  Y_pred=clustering.labels_
  sy=set(Y_pred)
  #print('len:', len(Y_pred))
  print('Classes:',len(sy),', min/max:',min(sy),'/',max(sy))
  if len(sy) < 6 and len(sy)>3 :
    print(pd.Series(Y_pred).value_counts())
  return X2, Y_pred
  
def graph(v1,v2):
  scatter=plt.scatter(v1[:,0], v1[:,1], c=v2)
  plt.legend(*scatter.legend_elements(), loc="upper right", title="Classes")
  plt.show()

def dbs0(c1, c2, ev, msv):
  X2 = df.iloc[:,[c1,c2]].to_numpy()
  clustering = DBSCAN(eps=ev, min_samples=msv).fit(X2)
  Y_pred=clustering.labels_
  sy=set(Y_pred)
  #print('len:', len(Y_pred))
  print('Classes:',len(sy),', min/max:',min(sy),'/',max(sy))
  if len(sy) < 6 and len(sy)>3:
    print(pd.Series(Y_pred).value_counts())
  return X2, Y_pred

dfc=df.copy()
dfc = dfc.reset_index(drop=True)
dfytest = pd.DataFrame(y,columns=['vals'])

def dbs02(c2, ev, msv):
  dftest2 = dfc.assign(vals=pd.Series(y).values)
  dftest3 = dftest2.iloc[:,[-1,c2]]
  dexdiff(dfytest.index, dftest3)
  X2 = dftest3.to_numpy()
  clustering = DBSCAN(eps=ev, min_samples=msv).fit(X2)
  Y_pred=clustering.labels_
  sy=set(Y_pred)
  print('Classes:',len(sy),', min/max:',min(sy),'/',max(sy))
  if len(sy) < 6:
    print(pd.Series(Y_pred).value_counts())
  return X2, Y_pred

#"""binfo"""
def binfo(sn,cn,x,dtx):
  with warnings.catch_warnings(record=True):

    print(f'dbscan 第{sn}分類,') #{cn}等於{x}
    dt2 = dtx[dtx['classes'].eq(sn)]
    dt3 = dfc.loc[dt2.index, wq]
    dexdiff(dt2.index, dt3)
    dtx3=pd.DataFrame(dt3)
    print(dtx3.groupby(wq).size(),'\n')
  #sn+=1

def dbwq(dqb=dtb):
  #dqb=b2
  bmax=max(set(dtb))+1
  dt = pd.DataFrame(dqb,columns=['classes'])

dtb,bmax,sn,db1=0,0,0,0
def binfoTest(dtb,t=2):
    if t == 1:
        #binfo-test01
        dt = pd.DataFrame(dtb,columns=['classes'])
        dt2 = dt[dt['classes'].eq(sn)].index
        dt3 = dfc.loc[dt2, wq]

        # binfo-test02
        coln=cName[db1]
        print(coln)
        try:
            colnuqa=df[cName[da1]].unique()
            print(len(colnuqa),'\n',np.mean(colnuqa))
        except:
            print()
        colnuqb=df[cName[db1]].unique()
        print(np.mean(colnuqb))
    
    else:
        for x in range(bmax):
            binfo(x,coln,'--',dt)
            print('==='*10,'\n')
        #dbwq()

# ratingDist func
dfc = df.copy() #uncomment for first use
dfc = dfc.reset_index(drop=True)
dfc[wtmp]=dfc[wtmp].round()

wq_rating=['丁','丙','乙','戊',]

def dexdiff(dy2,dy3):
  try:
    di=dy2.tolist()
    di3=dy3.index.tolist()
  except:
    print("try error")
    return
  difference = list(set(di) - set(di3))
  difflen=len(difference)
  if difflen != 0:
    print(len(di),len(di3))
    print(difflen,' diff!')

try:
    b2
except NameError:
    #var_exists = False
    b2=[]

def tempDist(sn=2,dtb=b2,dfww=wtmp,top5=False):
  if dtb.size == 0:
    return
  dt = pd.DataFrame(dtb,columns=['classes'])
  dt2 = dt[dt['classes'].eq(sn)].index
  dt3 = dfc.loc[dt2, wq]
  dexdiff(dt2,dt3)

  print(f'DBSCAN第{sn}分類中水質的{dfww}分布')
  iwq(dt3,dfww,top5)

def iwq(dt3,dfw=wtmp,top5=False):
  dfmerge = pd.DataFrame()
  for i in wq_rating:
    dt4 = dt3[dt3 == i].index
    #dfc = dfc.reset_index(drop=True)
    dtx2=dfc.loc[dt4, dfw]
    if dtx2.empty:
      continue
    dtx3=pd.DataFrame(dtx2)
    dtx4 = dtx3.groupby(dfw).size().rename_axis(dfw).reset_index(name=i)
    #dtx4 = dtx3.value_counts().rename_axis(wtmp).reset_index(name=i)
    #dtx4.rename(columns={'counts': i}, inplace=True)

    if dfmerge.empty:
      #print('hello')
      dfmerge = dtx4.copy()
    else:
      dfmerge = pd.merge(dfmerge, dtx4, on=dfw, how="outer")
  dfdisplay(dfmerge,top5)

def dfdisplay(dfmerge,top5=False):
  dfmerge.sort_values(dfmerge.columns[0],inplace=True)
  dfmerge.loc['Column_Total']= dfmerge.iloc[:, 1:].sum(numeric_only=True, axis=0)
  dfmerge.loc[:,'Row_Total'] = dfmerge.iloc[:, 1:].sum(numeric_only=True, axis=1)
  dfmerge.loc[dfmerge.index[-1], dfmerge.columns[0]] = 'col_total'
  #print(dfmerge.to_string(index=False),'\n')
  dfmerge.iloc[:, 1:-1] = dfmerge.iloc[:, 1:-1].div(dfmerge['Row_Total'], axis=0).mul(100).round(2).astype(str).add('%')
  dfmerge.replace('nan%',0, inplace=True)
  if top5:
    #print('top3')
    #df.nlargest(n=5, columns=['Magnitude', 'Depth'])
    #print(dfmerge.nlargest(n=4, columns=['Row_Total']).to_string(index=False))
    dtop5=dfmerge.iloc[:-2,-1].nlargest(n=3).index
    dtop5l=dfmerge.loc[dtop5, dfmerge.columns[0]].apply("{:g}".format).astype(str).values.tolist()
    print('top3 ',dtop5l)
    try:
      top5List.extend(dtop5l)
    except:
      print('err')
  else:
    print(dfmerge.to_string(index=False))
  print()


# '水溫_度C', '鎘', '水體分類等級'
#dtt=dfc.iloc[:,[2,13,1]]
try:
  cadv
except NameError:
  #cadv=pd.Series(df[df.columns[13]].unique())
  print()

try:
  b
except NameError:
  b=[]

def wqTemp2(dtb=b):
  dt = pd.DataFrame(dtb,columns=['classes'])
  bmax=max(set(dtb))+1
  print('DBSCAN分類中水質的鎘值分布\n')

  dfmerge = pd.DataFrame()
  for sn in range(bmax):
    dt2 = dt[dt['classes'].eq(sn)].index
    dt3 = dfc.loc[dt2, [wq, cad]]
    dexdiff(dt2,dt3)
    dcu=dt3[cad].unique()
    dcus='%.4f'%dcu[0]

    dtx2=dfc.loc[dt3.index, wq]
    if dtx2.empty:
      continue
    dtx3=pd.DataFrame(dtx2)

    dtx4 = dtx3.groupby(wq).value_counts().rename_axis(wq).reset_index(name=dcus)

    print(f'第{sn}分類中鎘值={dcus},水溫分布')
    iwq(dtx2)

    if dfmerge.empty:
      #print('hello')
      dfmerge = dtx4.copy()
      dfmerge.sort_values(dfmerge.columns[0],inplace=True)
    else:
      dfmerge = pd.merge(dfmerge, dtx4, on=wq, how="outer")
  #dfdisplay(dfmerge)
  print(dfmerge.to_string(index=False))


# CODE START HERE=================================
# 新增區段
cola=14
colb=19
X2 = df.iloc[:,[cola,colb]].to_numpy()
#X = pd.read_csv(input_file,header=None)
eps_grid = np.linspace(0.3,1.2,num=10)
silhouette_all = []

eps_best= eps_grid[0] # first num from eps_grid = 0.3
silhouette_score_max = -1
labels_best = None

for eps in eps_grid:
	model = DBSCAN(eps=eps, min_samples=6).fit(X2)
	labels = model.labels_
	silhouette_score = round(ss(X2, labels), 4)
	silhouette_all.append(silhouette_score)
	# print("Epsilon:", eps, " --> silhouette score:", silhouette_score)

	if silhouette_score > silhouette_score_max:
		silhouette_score_max = silhouette_score
		eps_best = eps
		labels_best = labels
		model_best = model

#print("Best epsilon & sil_score =",eps_best, silhouette_score_max           )
print('Best epsilon: %.4f' % eps_best)
print('sil_score: %.4f ' % silhouette_score_max)

labels = labels_best # TODO
model = model_best  # TODO

offset =0
if -1 in labels:
    offset = 1

num_clusters= len(set(labels)) - offset
print("Estimated number of clusters =",num_clusters )


#%%==
dfc=df.copy()
dfc = dfc.reset_index(drop=True)
dftest2 = dfc.assign(vals=pd.Series(y).values)
dftest3 = dftest2.iloc[:,[-1,1,6]]
X2 = dftest3.to_numpy()

dexdiff(dftest.index, dftest3)

#graph
zhfontsetting('sns')
num_rows = 4
num_cols = (len(df.columns) + num_rows - 1) // num_rows

#fig, ax = plt.subplots(num_rows, num_cols, sharey=True)
plt.figure(figsize=(15, 8))

for cx, col in enumerate(df.columns, 1):
  if cx<=c2 or cx>24: #>24
    continue
  plt.subplot(num_rows, num_cols, cx)
  xtest=df.iloc[:,[cx,c2]].to_numpy()
  plt.scatter(xtest[:,0], xtest[:,1])
  plt.xlabel('')
  plt.ylabel('wq')
  plt.title(f'col{cx} and wq',pad=20)

plt.tight_layout()
plt.show()

c1,c2=4,1
print(df.columns[[c1,c2]])
num_rows = 4
num_cols = (len(df.columns) + num_rows - 1) // num_rows

fig, axs = plt.subplots(num_rows, num_cols, sharey=True, figsize=(15, 8))
#fig.subplots_adjust()
print()
for cx,ax in enumerate(axs.flatten(),2):
  if cx<=c2 or cx>24:
    continue
  #ax.plot(np.random.random(100))
  xtest=df.iloc[:,[cx,c2]].to_numpy()
  ax.scatter(xtest[:,0], xtest[:,1])
  ax.grid(axis='y')
  ax.set_title(df.columns[cx],pad=10)

plt.tight_layout()
plt.suptitle(f'{wq}分布',y=1.05,size=25)
plt.show()

c1=2
c2=13
print(df.columns[[c1,c2]])
xtest=df.iloc[:,[c1,c2]].to_numpy()
plt.scatter(xtest[:,0], xtest[:,1])
plt.xlabel('水溫C')
plt.ylabel('鎘')
plt.title('鎘和水溫',pad=20)
plt.show()

# count

#dt = df.loc[df[df.columns[1]] == "丁"] # show related columns
dt = df[df.columns[1]] # only wq column
row = dt.loc[dt == "丁"]
#print(len(row),'\n',row.head(5))


## dbscan test ======================================
zhfontsetting('plt')
a,b= dbs(13, 2, 0.19, 4)
a2,b2= dbs0(13, 2, 0.19, 44)

print(cName[4]) #溶氧量_mg-L
db1=4
a,b= dbs02(db1, .6, 6)
dtb=b
graph(a,b)
binfoTest(dtb,1)
binfoTest(dtb)
## test03
coln=cName[db1]
dclas=[0,1,2,3]
top5List=[]
for idc in dclas:
  tempDist(idc,dtb,coln, True)
#print(set(top5List))

dclas=0
tempDist(dclas,dtb)

#wqTemp2(b)


#%%========kneed locator dbscan eps finder
#!pip install kneed
#@title dbscan kneed
da1=4
db1=6
neighbors = NearestNeighbors(n_neighbors=4)
dftest=X.iloc[:,[da1,db1]]
neighbors_fit = neighbors.fit(dftest)
distances, indices = neighbors_fit.kneighbors(dftest)

#Sort distance values by ascending value and plot
distances = np.sort(distances, axis=0)
k_dist = distances[:,1]

kneedle = KneeLocator(x = range(1, len(distances)+1), y = k_dist, S = 1.0,
                      curve = "concave", direction = "increasing", online=True)

# get the estimate of knee point
print(kneedle.knee_y) #36

#dbscan elbow
print('dbscan elbow plot')
from sklearn.neighbors import NearestNeighbors
#Calculate the average distance between each point in the data set and its 44 nearest neighbors
#20col*2 = 44
neighbors = NearestNeighbors(n_neighbors=4)
dftest=X.iloc[:,[da1,db1]]
#dftest=X.iloc[:,2:4]
neighbors_fit = neighbors.fit(dftest)
distances, indices = neighbors_fit.kneighbors(dftest)

#Sort distance values by ascending value and plot
distances = np.sort(distances, axis=0)
k_dist = distances[:,1]
plt.plot(k_dist)
plt.show()

#zoom in graph
plt.axis([3800, 4000, 0, 200])
plt.plot(k_dist)
plt.grid()
plt.show()

#%%==
eps = 10000
min_samples = 44
dbscan = cluster.DBSCAN(eps=eps, min_samples=min_samples)
clustering_labels = dbscan.fit_predict(dftest)

metrics.silhouette_score(X, clustering_labels)
#model received a Silhouette Coefficient score of 0.950.
#This is a decent score, indicating that my model doesn’t have overlapping clusters or mislabeled data points.


#%%==other test
#print(cName[0:7])
da1=4
db1=6
#arg3, arg4=.25,6
arg3, arg4=0.3,12
#arg3, arg4=.23,20
#arg3, arg4=.18,20
#arg3, arg4=0.3,24
a2,b2= dbs0(da1, db1, arg3, arg4)
#a2,b2= dbs(da1, db1, arg3, arg4) #scalar
dtb=b2

graph(a2,b2)
binfoTest(dtb,1)
binfoTest(dtb)
## test03
coln=cName[da1]

#class=2,b2,wtmp
dclas=0
tempDist(dclas,dtb)



#%%=====old test
exit()
aglist=[]
for agx in range(18,44):
  arg3, arg4=.11,agx
  a2,b2= dbs(da1, db1, arg3, arg4) #scalar
  b2len=len(set(b2))
  if b2len == 4 : # or b2len == 3:
    aglist.append([arg3,arg4])

print(aglist)

aglist=[[0.12, 60],[0.16, 20],[0.16, 22],[0.16, 38],[0.16, 60],[0.18, 20],[0.18, 22],[0.18, 38],[0.18, 80],[0.18, 82],[0.18, 84]]
#da1=4
#db1=3
#print(aglist[0][1])
for z in range(len(aglist)):
  arg3=aglist[z][0]
  arg4=aglist[z][1]
  a2,b2= dbs0(da1, db1, arg3, arg4)
  b2len=len(set(b2))
  if b2len == 4 or b2len == 3:
    graph(a2,b2)
  a2,b2= dbs(da1, db1, arg3, arg4) #scalar
  b2len=len(set(b2))
  if b2len == 4 or b2len == 3:
    graph(a2,b2)

fnum=[0.0014,0.0015]
for f in fnum:
  print(f'鎘等於{f}，')
  dt=df.loc[ np.isclose(df[cad], f), wq]
  dt2=pd.DataFrame(dt)
  print(dt2.groupby(wq).size(),'\n')

#b cad
a,b= dbs(13, 2, 0.19, 4)
a2,b2= dbs0(13, 2, 0.19, 44)

sn=2
dtb=b2 #b
coln=features[0] #cad
fnum=[0.0014,0.0015] #cad values
fnum=['20-30C','30-32C'] #temp range

coln=features[3] #
fnum=['0']

dt = pd.DataFrame(dtb,columns=['classes'])

for x in fnum:
  binfo(sn,coln,x,dt)
  sn+=1

#b
#tempDist(0,b)
#wqTemp2(b)

#temp range and wq
fnum=[[20,30],[30,32]]
for x in range(2):
  x1=fnum[x][0]
  x2=fnum[x][1]
  print(f'水溫{x1}到{x2}，')
  dt=df.loc[ df[wtmp].between(x1, x2, inclusive='neither'), wq]
  dt2=pd.DataFrame(dt)
  print(dt2.groupby(wq).size(),'\n')

print(len(b2))

sn=2
dtb=b2
t=[]
dt = pd.DataFrame(dtb,columns=['classes'])
#print(len(dt))
dt2 = dt[dt['classes'].eq(sn)]
dt3 = df['水溫_度C'].iloc[dt2['classes'].index]
print(f'b2 class {sn}, total:',len(dt3))
print('max/min temp',max(dt3),'/',min(dt3))

dt3 = df[cad].iloc[dt2['classes'].index]
print(dt3.eq(0).sum())
#dt4 = dt3.eq(0).index
dt4 = df[wq].loc[dt3.eq(0).index]
print(dt4)


#%===

"""

```
DBSCAN第0分類中水質的懸浮固體_mg-L分布
top3  ['2.5', '8', '7.2']

DBSCAN第1分類中水質的懸浮固體_mg-L分布
top3  ['2.5', '3.6', '4.8']

DBSCAN第2分類中水質的懸浮固體_mg-L分布
top3  ['13.1', '15.2', '25.2']

DBSCAN第3分類中水質的懸浮固體_mg-L分布
top3  ['2.5', '6.6', '3']

10 {'3.6', '4.8', '25.2', '7.2', '8', '3', '2.5', '13.1', '6.6', '15.2'}

 ====================================

DBSCAN第0分類中水質的導電度分布
top3  ['396', '402', '405']

DBSCAN第1分類中水質的導電度分布
top3  ['337', '360', '381']

DBSCAN第2分類中水質的導電度分布
top3  ['422', '430', '523']

DBSCAN第3分類中水質的導電度分布
top3  ['278', '325', '360']

11 {'381', '405', '402', '360', '523', '422', '278', '325', '396', '337', '430'}

 ====================================

DBSCAN第0分類中水質的氨氮_mg-L分布
top3  ['0.09', '0.08', '0.14']

DBSCAN第1分類中水質的氨氮_mg-L分布
top3  ['0.06', '0.12', '0.08']

DBSCAN第2分類中水質的氨氮_mg-L分布
top3  ['0.22', '0.27', '0.06']

DBSCAN第3分類中水質的氨氮_mg-L分布
top3  ['0.05', '0.03', '0.04']

10 {'0.14', '0.06', '0.03', '0.12', '0.04', '0.05', '0.08', '0.22', '0.27', '0.09'}

 ====================================
```

"""



#%%==
"""*b*
```
水體分類等級
丁    3208
丙     321
乙     154
戊     309

Classes: 5 , min/max: -1 / 3
 0    3473
 3     231
 1     180
 2      95
-1      13

鎘等於0.0014，
水體分類等級
丁    154
丙     19
乙      5
戊      5
dtype: int64

第1分類
 水體分類等級  0.0014
0      丁     152
1      丙      19
2      乙       5
3      戊       4

鎘等於0.0015，
水體分類等級
丁    176
丙     36
乙     11
戊      9
dtype: int64

dbscan 第2分類, 鎘等於0.0014
水體分類等級
丁    66
丙    21
乙     4
戊     4
dtype: int64

dbscan 第3分類, 鎘等於0.0015
水體分類等級
丁    175
丙     36
乙     11
戊      9

  水體分類等級  0.0015
0      丁     175
1      丙      36
2      乙      11
3      戊       9
```

*b2*
```
Classes: 5 , min/max: -1 / 3
 2    3273
-1     439
 3     127
 1      78
 0      75

水溫20到30，
水體分類等級
丁    2611
丙     239
乙     114
戊     233

水溫30到32，
水體分類等級
丁    227
丙     37
乙     22
戊     29

dbscan 第2分類, 水溫_度C等於20-30C
水體分類等級
丁    2652
丙     264
乙     124
戊     233

第2分類中水質的水溫分布
    水溫_度C      丁     丙     乙     戊  Row_Total
     21.0   84.0   3.0   4.0  10.0      101.0
     22.0  250.0  15.0   9.0  17.0      291.0
     23.0  226.0  12.0   5.0  29.0      272.0
     24.0  276.0  26.0  10.0  27.0      339.0
     25.0  301.0  30.0  13.0  20.0      364.0
     26.0  377.0  24.0  12.0  33.0      446.0
     27.0  309.0  22.0  18.0  16.0      365.0
     28.0  310.0  37.0  16.0  23.0      386.0
     29.0  252.0  52.0  14.0  30.0      348.0
     30.0  220.0  39.0  19.0  22.0      300.0
     31.0   47.0   4.0   4.0   6.0       61.0
col_total 2652.0 264.0 124.0 233.0     3273.0

    水溫_度C       丁       丙      乙       戊  Row_Total
     21.0 83.17 %  2.97 % 3.96 %   9.9 %      101.0
     22.0 85.91 %  5.15 % 3.09 %  5.84 %      291.0
     23.0 83.09 %  4.41 % 1.84 % 10.66 %      272.0
     24.0 81.42 %  7.67 % 2.95 %  7.96 %      339.0
     25.0 82.69 %  8.24 % 3.57 %  5.49 %      364.0
     26.0 84.53 %  5.38 % 2.69 %   7.4 %      446.0
     27.0 84.66 %  6.03 % 4.93 %  4.38 %      365.0
     28.0 80.31 %  9.59 % 4.15 %  5.96 %      386.0
     29.0 72.41 % 14.94 % 4.02 %  8.62 %      348.0
     30.0 73.33 %  13.0 % 6.33 %  7.33 %      300.0
     31.0 77.05 %  6.56 % 6.56 %  9.84 %       61.0
col_total 81.03 %  8.07 % 3.79 %  7.12 %     3273.0

dbscan 第3分類, 水溫_度C等於30-32C
水體分類等級
丁    94
丙    13
乙     7
戊    13
```
"""