import pandas as pd
import numpy as np
from sklearn.preprocessing import LabelEncoder
le = LabelEncoder()
from matplotlib.font_manager import fontManager
import seaborn as sns
import matplotlib as mpl
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split

df=pd.read_csv('mydata.csv',encoding='UTF-8',index_col=False)
# df = df.drop('index', axis=1)
print(df.head(5))
print(df.dtypes)

#水體等級 water_quailty
wq='水體分類等級'
print(df.groupby(wq).size())

print(df[df.columns[1]].unique())
df[wq]= le.fit_transform(df[wq])
print(df[wq].unique())
print(df.groupby(wq).size())

# linearSVC==
print('linearSVC')
cName = df.columns.tolist()
features = cName[2:] #['水溫_度C', 'pH值', '溶氧量_mg-L']
print(features)
#features.append(cName[8])
X_train, y_train = df[features].values, df[wq].values

from sklearn.preprocessing import StandardScaler
scalar=StandardScaler()
scalar.fit(X_train)
X_train_std=scalar.transform(X_train)

from sklearn.svm import LinearSVC
model = LinearSVC(C=0.1, dual=False, class_weight='balanced')
model.fit(X_train_std, y_train)

y_pred = model.predict(X_train_std)
print('y_train len:',len(y_train)) #3992
print('Miscalssified samples: %d' % (y_train != y_pred).sum())
# Miscalssified samples: 803

from sklearn.metrics import accuracy_score
print('Accuracy: %.4f' % accuracy_score(y_train, y_pred) )
# Accuracy: 0.7988

# 計算有加權的 F1-score (weighted)
from sklearn.metrics import f1_score
f1 =f1_score(y_train, y_pred, average='weighted')
print('F1-score: %.4f' % f1)
# F1-score: 0.7415

undersampler = 'AllKNN'
undersampling = True
# X_train, X_test, y_train, y_test = split(X, y, test_size=test_size, threshold_entropy=threshold_entropy, undersampling=undersampling, undersampler=undersampler, random_state=random_state)

# colab set chinese font
# TaipeiSansTCBeta-Regular.ttf 
def zhfontsetting(p):
    if p == 'sns':
        plt.style.use('seaborn-v0_8')
    elif p == 'plt':
        mpl.rcParams.update(mpl.rcParamsDefault)
    #plt.rcParams['axes.unicode_minus'] = False
    fontManager.addfont('TaipeiSansTCBeta-Regular.ttf')
    mpl.rc('font', family='Taipei Sans TC Beta')


zhfontsetting('sns')

num_rows = 3
num_cols = (len(df.columns) + num_rows - 1) // num_rows  # Calculate the number of columns dynamically

plt.figure(figsize=(15, 8))

for i, col in enumerate(df.columns, 1):
    plt.subplot(num_rows, num_cols, i)
    sns.histplot(df[col], kde=True)
    plt.xlabel('')
    plt.ylabel('數量')
    plt.title(f'{col} 分配')

plt.tight_layout()
plt.show()

print(max(df[df.columns[7]]),min(df[df.columns[7]]))

plt.figure(figsize=(3,2))
sns.histplot(df[df.columns[7]], kde=True, bins=15)
plt.xlabel('')
plt.show()

print(len(cName))


#heatmap
#sns.reset_defaults()
sns.set(font=zhfont.get_name(), font_scale=1.7)

X = df.drop(columns=[wq])  # Features
y = df[wq]  # Target

# Split the data into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=33)

# Create a correlation matrix
corr = X.corr()

plt.figure(figsize=(20, 12))  # Set the figure size (adjust values as needed)

# Create a heatmap from the correlation matrix
#sns.heatmap(corr, annot=True)
sns.heatmap(corr, cmap='RdBu', vmin=-1, vmax=1, annot=True, annot_kws={'fontsize':20, 'fontweight':'bold'})
# Show the heatmap
plt.show()


zhfontsetting('plt')
zhfont = mpl.font_manager.FontProperties(fname='/content/TaipeiSansTCBeta-Regular.ttf')
#from yellowbrick import style as ys
#ys.rcmod.set_aesthetic(font=zhfont, font_scale=1, color_codes=True, rc=None)
from sklearn.ensemble import RandomForestClassifier
from yellowbrick.model_selection import feature_importances
from yellowbrick.contrib.missing import MissingValuesBar, MissingValuesDispersion
from yellowbrick.target.feature_correlation import feature_correlation

X = df.drop(columns=[wq])  # Features
y = df[wq]
feature_importances(RandomForestClassifier(criterion='gini', n_estimators=50), X, y, fontproperties=zhfont);

from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import LogisticRegressionCV

feature_importances(LogisticRegression(penalty='elasticnet', solver='saga', l1_ratio=0.5), X, y);

print(df[wq].unique())
print(le.inverse_transform(df[wq].unique()))
#[0 1 3 2]
#array(['丁', '丙', '戊', '乙'], dtype=object)


from sklearn.decomposition import PCA
# Initialize PCA and fit to the data
pca = PCA()
pca.fit(X_train)

# Plotting the explained variance ratio
# plt.figure(figsize=(8, 6))
plt.figure(figsize=(5, 3))
plt.plot(range(1, len(pca.explained_variance_ratio_) + 1),
         pca.explained_variance_ratio_.cumsum(), marker='o', linestyle='--')
plt.xlabel('X自變數')
plt.ylabel('累積解釋方差')
plt.title('解釋的應變數與自變數',pad=5)
#plt.xlabel('Number of Components')
#plt.ylabel('Cumulative Explained Variance')
#plt.title('Explained Variance vs. Number of Components')
plt.grid(True)
plt.show()


#dtree
from sklearn.model_selection import train_test_split as tts
from sklearn import tree, cluster

features = cName[2:] #0.837
#features = cName[3:5]
#features = cName[5:8] #[cName[6],cName[-1]]
# features = [cName[5],cName[11]] #0.833
# features = cName[8]
print(features)
X = pd.DataFrame(df[features])
#y = le.fit_transform(df[wq])

print("\ndtree")
XTrain, XTest, yTrain, yTest = tts(X,y, random_state=1)
dtree= tree.DecisionTreeClassifier(max_depth=3)
dtree.fit(XTrain, yTrain) # training model
print('accuracy: %.3f' % (dtree.score(XTest, yTest)))
#accuracy: 0.837

for index, value in enumerate(cName):
   print(index, value)

print(X.columns)

dftest=X.iloc[:,2:4]
print(dftest.head(4))

#%%==
zhfontsetting('plt')
plt.figure(figsize=(15, 8))
for i, col in enumerate(df.columns[1:], 1):
  plt.subplot(4, 2, i)
  xtest=df.iloc[:,[4,i]].to_numpy()
  plt.scatter(xtest[:,0], xtest[:,1])
  plt.xlabel('')
  plt.ylabel('')
  plt.title(f'{df.columns[i]}')
print('====')
plt.suptitle('溶氧量與其他參數關聯')
plt.tight_layout()
plt.show()

exit()
#%%==
"""
```
# Classes: 5 , min/max: -1 / 3
 0    3473
 3     231
 1     180
 2      95
-1      13
dtype: int64

水體分類等級
丁    3208
丙     321
乙     154
戊     309
dtype: int64

y (labelEncoder)
0    3208
1     321
2     154
3     309
```

"""
#test
print(set(y))
print(df.groupby(wq).size())
dt=pd.DataFrame(y,columns=['vals'])
print(dt.groupby('vals').size())
#['丁' '丙' '戊' '乙']
dt=pd.DataFrame(b,columns=['vals'])
print(dt.groupby('vals').size())

#titanic.loc[titanic["Age"] > 35, "Name"]
dt=df.loc[ df[df.columns[13]].between(0.00135, 0.00145, inclusive='neither'), wq]
#print(dt.head(5))
#print(type(dt)) #series
dt2=pd.DataFrame(dt)
print(dt2.groupby(wq).size())

#print(np.unique(b))
b2=np.where(b == 2)[0]
print(len(b2))

dt=df[df.columns[13]].between(0.00135, 0.00145, inclusive='neither')
my_value = True
rows = dt.loc[dt == my_value]
print(len(rows),'\n',rows.head(5))