#data extract
import numpy as np
import pandas as pd
import csv
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

def Average(lst):
    return sum(lst)/len(lst)

#"""function compare df df2 column data"""
def mergedd(x,y=0):
  if y == 0:
    dfc=df2[x]
  else:
    dfc=dftest[x]
  mergeS = pd.DataFrame(zip(df[x], dfc), columns=['original', 'processed'])
  print(mergeS[0:10],'\n')
  print(mergeS[180:200],'\n')
  print(mergeS[1500:1520],'\n')
  print(mergeS[4035:4042])


df=pd.read_csv('臺中市河川水質監測112.12.csv.csv',encoding='UTF-8')
cName = df.columns.tolist()
wq='水體分類等級'
wq_rating=['丁','丙','乙','戊']

# df.head(5)

#print(cName)

#data info
print('==== dataframe info ====')
print('df.shape',df.shape) #(4042, 35)
print()
print(len(df['監測站編號'].unique()),'個不同監測站')
# print('各監測站檢測次數===\n',df.groupby('監測站編號').size())
print()
print(df[cName[4]].iloc[[0,-1]]) #date range
print('各分級統計====\n',df.groupby(wq).size())
print()
print(df.info(),'\n')
print()
#print('空白欄位統計====\n',df.isna().sum()) #count empty columns
#print(df.eq(0).any().any()) # find value=0 #False

#check column with missing vals
colList=[]
col2List=[]
col3List=[]
c2Name=[]
c2Name=[]
dflen=len(df.index) #4042
print('總行數:',dflen)
print('columns with missing values, available data entry count')
for i in range(len(cName)): #col in df.columns:
  col=cName[i]
  gloc=i #df.columns.get_loc(col) #column index number
  empty=df[col].isna().sum() #each column missing entry count
  if empty !=0:
    left=dflen-empty
    if left ==0:
      col3List.append(col) #完全空白
      continue
    print(gloc,col,'只有:',left,'筆')
    col2List.append(gloc) #有些缺值
    c2Name.append(cName[i])
  else:
    colList.append(gloc) #無缺值

#print('\ncolumn index')
colsList=colList+col2List #sorted(colsList)
#print('w/ missing ',col2List,'\nfull ',colList,'\ncombine ',colsList)
print('欄位完全空白:\n',col3List) #completely empty columns
#print(len(colsList))
#print(len(cName)-len(col3List))

dfc=df.copy()
print('missin vals wq counts')
dfmerge = pd.DataFrame()
dfmerge2 = pd.DataFrame()

for i in col2List: #[0:3]
  col=i #col2List[1]
  coln=cName[col] #dfc.columns[col]
  dfg=dfc.iloc[:,[3,col]]
  dfmc=dfg.drop(wq, 1).isna().groupby(dfg[wq], sort=False).sum().reset_index().rename(columns={coln:f'無{coln[:3]}'})
  dfnmc=dfg.drop(wq, 1).notna().groupby(dfg[wq], sort=False).sum().reset_index().rename(columns={coln:f'有{coln[:2]}'})
  dfmerge2 = pd.merge(dfmc, dfnmc, on=wq, how="outer")

  if dfmerge.empty:
    dfmerge = dfmerge2.copy()
  else:
    dfmerge = pd.merge(dfmerge, dfmerge2, on=wq, how="outer")

dfmerge.loc['Column_Total']= dfmerge.iloc[:, 1:].sum(numeric_only=True, axis=0)
dfmerge.loc[dfmerge.index[-1], dfmerge.columns[0]] = 'col_total'
print(dfmerge.to_string(index=False))
#display(dfmerge)
#dfmerge.to_csv('/content/missingWqCounts.csv', index=False, quotechar='"', quoting=csv.QUOTE_NONNUMERIC, encoding='utf-8')

#"""# find uniq"""
coln=cName[21]
dtest = dfc[coln]
dtest2 = dtest[dtest.str.contains('E\+07',na=False)]
print(coln,dtest2.unique())

lessSign=[]
for col in range(5,10):
  coln=cName[col]
  dtest = dfc[coln]
  dtest2 = dtest[dtest.str.contains('<',na=False)]
  print(coln,dtest2.unique())
  lessSign.extend(dtest2.unique())
print(cName[5:10],'vals contain <')
print(lessSign)


#"""# make df2"""

#colsList=[2,3,8,9,10,24,13,15,27] #,12,25 #pdf
#colsList=[2,3,8,9,10,13,14,15,24,27,7,11,12,16,17,18,19,20,21,22,23,25,28,32,33]
colsList=[2,3,8,9,10,13,14,15,24,27,12,25]

df2=df.iloc[:,colsList]
print(df2.info())
#c2Name = df2.columns.tolist()
df2.to_csv('/content/mydata.csv', index=False, quotechar='"', quoting=csv.QUOTE_NONNUMERIC, encoding='utf-8')
df2=pd.read_csv('/content/mydata.csv', encoding='UTF-8',engine='python')
dfc=df2.copy()

#"""導電度、生化需氧量、懸浮固體、氨氮 to numeric"""

#use csvviewer to sort each column to see data with string
#df2.iloc[746:750,5]
#pd.reset_option('^display.', silent=True)
##pd.options.display.precision = 3 #default=6

col=8 #導電度
coln=cName[col]
#if df2.iloc[:,col].dtype == 'object':
if df2[coln].dtype == 'object':
  try:
    dtest = df2[coln].apply(lambda x: str(x).replace(',','').strip()).to_frame()
    df2[coln] = pd.to_numeric(dtest.squeeze())
  except ValueError:
    print('error col:', col,'\n')

#convert col 生化需氧量、化學需、懸浮固體、氨氮
pd.options.display.float_format = '{:,.2f}'.format
lessSign=[]
for col in range(5,10):
  coln=cName[col]
  dtest = df2[coln]
  if dtest.dtype != 'object':
    continue
  dtest2 = dtest[dtest.str.contains('<')]
  lessSign.extend(dtest2.unique())

print('pd.eval is slow')
for col in range(5,10):
  coln=cName[col]
  if df2[coln].dtype == 'object':
    try:
      for i in lessSign:
        if i == '<0.01':
          sub='-0.01+'
        else:
          sub='-0.1+'
        dtest = df2[coln].replace('<',sub,regex=True)
      df2[coln] = pd.DataFrame(dtest).applymap(pd.eval) #slow
    except ValueError:
      print('error col:', col,'\n')
print(df2[df2.columns[5:10]].dtypes)

#總磷
coln=cName[12]
if df2[coln].dtype == 'object':
  df2[coln] = pd.to_numeric(df2[coln].str.replace('ND','0'), errors='coerce')
#print(dtest[~dtest.applymap(np.isreal).all(1)])
print(coln, df2[coln].dtypes)

cv={'丁':7.91,'丙':10.72,'戊':1.39,'乙':0.04}
for rating,val in cv.items():
  dci=df2.loc[(df2[coln].isna()) & (df2[wq]==rating)].index
  if len(dci) > 1:
    df2.loc[dci, coln] = val #replace nan w/ 0.04
print()

#21  大腸桿菌群_CFU
pd.reset_option('^display.', silent=True)
coln=cName[21]
if df2[coln].dtype == 'object':
  dtest = pd.to_numeric(df2[coln].str.replace('TNTC','9.50E+07').replace("<",''), errors='coerce')
  df2[coln] = pd.to_numeric(dtest, downcast='float')

cv={'丁':752600.2, '丙':151221.7, '戊':1281717.7, '乙' :71388.0}
for rating,val in cv.items():
  dci=df2.loc[(df2[coln].isna()) & (df2[wq]==rating)].index
  if len(dci) > 1:
    df2.loc[dci, coln] = val #replace nan w/ 0.04

print(df2[coln].isnull().sum())
#df2.loc[(df2[coln].isna()) & (df2[wq]=='0')]
#print(df2.dtypes)


#dfc=df2.copy()
# 總磷 data range
print('總磷 data range')
coln=cName[12]
print(coln,'= 乙 data range, 乙標準0.05以下')

dfg=dfc.groupby(by=[coln, wq]).size().reset_index()
dfg.rename(columns={0: 'counts'}, inplace=True)
#print(dfg)
dfgwqB=dfg.loc[dfg[wq] == '乙']
cvals=dfgwqB.iloc[:,0].tolist()
print(min(cvals),max(cvals)) #0.006 ND
# cvals.remove('ND')
print(min(cvals),max(cvals)) #0.006 78.4
#cvalsint = [float(x) for x in cvals]
# print(Average(cvalsint))
print('avg vals ====')
wq_rating=['丁','丙','戊','乙']
for i in wq_rating:
  dfgwqB=dfg.loc[dfg[wq] == i]
  cvals=dfgwqB.iloc[:,0].tolist()
  print(i,Average(cvals))
print()

#check row
dcheck=df.loc[ (df[coln] == '78.4') & (df[wq] == '乙') ]
print(dcheck.dropna(axis=1, how='all')) #drop na columns

#dcheck.to_csv('/content/rowcheck.csv', index=False, quotechar='"', quoting=csv.QUOTE_NONNUMERIC, encoding='utf-8')

# #sort 乙 counts top 5
# dfgwqB.sort_values(dfgwqB.columns[-1],ascending=False,inplace=True)
# print(dfgwqB)

# 數值分布
plt.figure(figsize=(5, 3))
#plt.bar(range(len(frequencies)), frequencies, align='center')
xtest=dfgwqB.iloc[:,[0,-1]].to_numpy()
fig, ax = plt.subplots()
ax.scatter(xtest[:,0], xtest[:,1])
ax.set_ylabel('counts')
ax.xaxis.set_major_locator(ticker.MaxNLocator(5, min_n_ticks=3))
plt.show()
print()

#21  大腸桿菌群_CFU
print('大腸桿菌群 data range')
coln=cName[21]
dfg=dfc.groupby(by=[coln, wq]).size().reset_index()
print('avg vals ====')
wq_rating=['丁','丙','戊','乙']
for i in wq_rating:
  # break
  dfgwqB=dfg.loc[dfg[wq] == i]
  cvals=dfgwqB.iloc[:,0].tolist()
  print(i,Average(cvals))
  print(min(cvals),max(cvals))

dcheck=df.loc[df[coln] == 'TNTC' ]
print(dcheck.dropna(axis=1, how='all')) #drop na columns

print()
#mergedd('氨氮_mg-L',2)
mergedd('生化需氧量_mg-L',2)

coln='導電度'
dotest=pd.DataFrame(df2[coln])
dptest=pd.DataFrame(df[coln])
dexdiff(dotest, dptest)
mergedd('導電度')
print()

#"""# export df2toCSV"""
print(df2[wq].isna().sum())
dfzero=df2.loc[df2[wq] == '0']#.reset_index()
dfzero.to_csv('/content/zerodata.csv', index=False, quotechar='"', quoting=csv.QUOTE_NONNUMERIC, encoding='utf-8')

#remove wq=0 rows
df2[wq].replace('0', np.nan, inplace=True)
df2.dropna(axis=0, subset=[wq], inplace=True)

df2.to_csv('/content/mydata.csv', index=False, quotechar='"', quoting=csv.QUOTE_NONNUMERIC, encoding='utf-8')

#load df2 csv
dftest=pd.read_csv('/content/mydata.csv',encoding='UTF-8')
print(dftest.info())

#%%==
exit()
#test
cols = df.columns.values
print(cols)
[df.columns.get_loc(c) for c in cols if c in df]
(df.columns == cols).nonzero()

