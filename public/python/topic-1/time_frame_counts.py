import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

df=pd.read_csv('每年新北市A2交通事故時間別.csv',header=0,encoding='UTF-8')
ot='瑞芳分局'
#mask = (df['organ'] == '土城分局')
mask = (df['organ'] == ot)
branch=df[mask]
#print(len(branch.index))
#rt_col=branch.iloc[:,2:]

years=[]
count=len(branch.index)
mm=[]
for i in range(count):
    years.append(branch.iloc[i,2:])
    #mm.append(sum(years[i])/12)
    #mm.append(max(years[i]))
    #mm.append(min(years[i]))
#print(max(mm),min(mm))
#print(sum(mm)/6)
#max:  295.0 , min:  15.0 , mean: 147

x=np.arange(1,13,1)
x = ["0-2", "2-4", "4-6", "6-8",'8-10','10-12','12-14','14-16','16-18','18-20','20-22','22-24' ]
nm='99'
beingsaved = plt.figure(figsize = (6,4),dpi=150)
for i in range(count):    
    plt.plot(x,years[i],'o--',label=nm,linewidth=1, markersize=4)
    nm=str(100+i)

plt.rcParams['font.family']='Microsoft Yahei'
#plt.title("土城分局各年時段", pad=20,size=24)
pt=ot+'各年時段'
plt.title(pt, pad=20,size=24)
plt.xlabel("時段",size=16)
plt.ylabel("數量",size=16)
plt.xticks(rotation=45, ha='right')
plt.legend(title='年分',title_fontsize=16,bbox_to_anchor=(1.2, 1),loc='upper right', shadow=True) 
plt.grid() #show grid
plt.show() #tucheng-yearly.png

#%%
#https://matplotlib.org/stable/gallery/subplots_axes_and_figures/subplots_demo.html
#yr99 and yr104 time frames average piecharts.
#yr99-104-avg_piechart.png
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

df=pd.read_csv('每年新北市A2交通事故時間別.csv',header=0,encoding='UTF-8')
#df['organ'] = df['organ'].replace(['中和分局'], '中和第一分局')

times=df.groupby('year',sort=False)\
["time1","time2","time3","time4","time5","time6","time7","time8","time9","time10","time11","time12"].mean()
print(times.index.size) #6
#print(times.iloc[0,:].astype(int)) #yr99 each time frames' means
#print(times.iloc[5,:].astype(int)
timesAvg=[list(times.iloc[0,:]), list(times.iloc[5,:].astype(int))]
print(timesAvg,'\n\n')
#[[35.375, 15.375, 17.25, 105.4375, 153.4375, 105.625, 114.6875, 112.4375, 141.5625, 131.0, 107.6875, 87.125],
# [54, 22, 24, 180, 283, 222, 212, 205, 257, 239, 171, 143]]
print(len(timesAvg[0])) #12
print(len(timesAvg[1])) #12

#print( max(timesAvg[0]), max(timesAvg[1]) )
#top3-time-frames-avg_yr99-104.png
labels= ["0-2", "2-4", "4-6", "6-8",'8-10','10-12','12-14','14-16','16-18','18-20','20-22','22-24' ]
for _ in range(2):
    top3=sorted(timesAvg[_], reverse=True)[0:3]
    print(99+_,'年')
    for i in top3:
        #print(i)
        ind=timesAvg[_].index(i)
        print('value:',i ,', time:', labels[ind],', index:',ind) 
    print('~'*17)
    #[44, 17, 19, 115, 181, 112, 130, 132, 155, 135, 150, 100] val: 181, index: 4, time: 8-10 am
#==
#https://stackoverflow.com/questions/30328646/group-by-in-group-by-and-average
#sort by index 2 of nested list, https://stackoverflow.com/questions/65679123/sort-nested-list-data-in-python

fig, (ax1,ax2) = plt.subplots(1,2,figsize=(10,10),dpi=144)
#plt.figure(figsize =(10, 7))
plt.rcParams['font.family']='Microsoft Yahei'

#labels= ["0-2", "2-4", "4-6", "6-8",'8-10','10-12','12-14','14-16','16-18','18-20','20-22','22-24' ]
explode = (0,0,0,0, 0.15,0,0,0, 0.15,0.07,0,0)
ax1.pie(timesAvg[0], explode=explode, labels = labels, autopct= "%1.1f%%", counterclock=False, startangle=230)
#ax1.axis('equal') #set axis?
ax1.set_title('99年',pad=25,size=20,fontweight='bold')

ax2.pie(timesAvg[1], explode=explode, labels = labels, autopct= "%1.1f%%", counterclock=False, startangle=230)
ax2.set_title('104年',pad=25,size=20,fontweight='bold')

plt.suptitle('各時段A2事故平均值',y=.8,size=25) #fig.suptitle
plt.show()
#https://www.geeksforgeeks.org/plot-a-pie-chart-in-python-using-matplotlib/
#https://matplotlib.org/stable/api/_as_gen/matplotlib.axes.Axes.pie.html#matplotlib.axes.Axes.pie
#https://stackoverflow.com/questions/55767312/how-to-position-suptitle
#https://medium.com/@kvnamipara/a-better-visualisation-of-pie-charts-by-matplotlib-935b7667d77f
#https://stackoverflow.com/questions/58887571/plotting-2-pie-charts-side-by-side-in-matplotlib
#https://stackoverflow.com/questions/66359171/plotting-two-pie-charts-one-beside-another-one-subplots
#colors=['yellowgreen','gold','skyblue','lightcoral']
#colors=colors
#https://techoverflow.net/2021/04/04/how-to-fix-matplotlib-title-typeerror-text-object-is-not-callable/
#https://stackoverflow.com/questions/23577505/how-to-avoid-overlapping-of-labels-autopct-in-a-matplotlib-pie-chart