'''
https://data.gov.tw/dataset/124159
year(年度)、organ(機關別)、time1(0-2時)、time2(2-4時)、time3(4-6時)、time4(6-8時)、time5(8-10時)、time6(10-12時)、time7(12-14時)、time8(14-16時)、time9(16-18時)、time10(18-20時)、time11(20-22時)、time12(22-24時)
'''

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

col = ["year","organ","time1","time2","time3","time4","time5","time6","time7","time8","time9","time10","time11","time12"]
data=[["99年","土城分局","39.0","15.0","23.0","103.0","156.0","115.0","128.0","149.0","173.0","141.0","155.0","102.0"],
["99年","中和第二分局","49.0","20.0","16.0","127.0","207.0","109.0","132.0","116.0","137.0","129.0","145.0","98.0"],
["100年","土城分局","64.0","17.0","26.0","186.0","226.0","142.0","166.0","149.0","239.0","237.0","154.0","133.0"],
["100年","中和第二分局","45.0","17.0","22.0","119.0","185.0","132.0","108.0","107.0","142.0","171.0","123.0","118.0"]
]

'''
df = pd.read_csv("每年新北市A2交通事故時間別",header=0,encoding='UTF-8')
y1=df.head(17)
'''
y1 = pd.DataFrame(data, columns=col)
#print('max: ',y1.max(),'min: ', y1.min(),'sum: ', y1.sum())
#y1 = y1.infer_objects()
#print(y1.dtypes)
y1[y1.columns[2:]]=y1[y1.columns[2:]].astype(float)
print(y1.dtypes)
#https://stackoverflow.com/questions/52395179/change-dtype-of-specific-columns-with-iloc


print(y1.iloc[0,2:]) #99年 土城分局 times...
#print(y1.info())
print(len(y1.columns)) #14 columns
'''
total=0
for column in y1.iloc[0,2:]:
  #print(column)
  total += float(column)
print(total)
'''
'''
n=y1.iloc[0,2:].astype(float)
#print(*n,sep="+")
print(int(n.sum()))
#print(y1.iloc[0:1,0:2] )
print( y1.iloc[[0],[0,1]] )
'''    
rowTotal=[]
for i in y1.index:
    #print(y1.iloc[[i],[0,1]])
    n=y1.iloc[i,2:] #.astype(float)
    rowTotal.append(int(n.sum()))
y1['rowTotal']=rowTotal #add column 小計
print(y1.iloc[ :, [0,1,-1] ]) #display year, organ, rowtotal

#https://stackoverflow.com/questions/27160565/reading-csv-file-in-python-and-summing-the-values-in-the-row
#https://sparkbyexamples.com/pandas/pandas-sum-rows/
#https://www.geeksforgeeks.org/how-to-convert-string-to-integer-in-pandas-dataframe/
#https://www.geeksforgeeks.org/adding-new-column-to-existing-dataframe-in-pandas/
#https://www.geeksforgeeks.org/different-ways-to-iterate-over-rows-in-pandas-dataframe/

#https://sparkbyexamples.com/pandas/pandas-groupby-sum-examples/
#https://sparkbyexamples.com/pandas/pandas-convert-string-to-float-type-dataframe/
#https://www.geeksforgeeks.org/change-data-type-for-one-or-more-columns-in-pandas-dataframe/
#https://stackoverflow.com/questions/55439027/rangeindex-object-is-not-callable

#https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.legend.html
#https://ithelp.ithome.com.tw/articles/10201670
#https://matplotlib.org/3.1.1/gallery/misc/plotfile_demo.html#sphx-glr-gallery-misc-plotfile-demo-py
#https://pythonguides.com/matplotlib-x-axis-label/
    #Matplotlib x-axis label string
#https://pythonguides.com/put-legend-outside-plot-matplotlib/
#https://blog.csdn.net/Poul_henry/article/details/82533569
#https://stackoverflow.com/questions/10998621/rotate-axis-text-in-matplotlib
#https://stackoverflow.com/questions/19125722/adding-a-matplotlib-legend
#https://jakevdp.github.io/PythonDataScienceHandbook/04.06-customizing-legends.html#Multiple-Legends

#time1 sum by year
#covnvert colum to float
colNm='time1'
print(y1.groupby('year', sort=False)[colNm].sum())

#times=len(y1.loc[0,'time1':'time12']) #12
#'year', as_index =False
times=y1.groupby('year',sort=False)\
["time1","time2","time3","time4","time5","time6","time7","time8","time9","time10","time11","time12"].sum()
print(times.iloc[1,:]) #Name: 100年
#print(times.info())
'''
          time1  time2  time3  ....
year                                    
99年    88.0   35.0   39.0
100年  109.0   34.0   48.0  
'''
#y99=list(times.iloc[0,:])
#y100=list(times.iloc[1,:])
#print(y100)

years=[]
for i in range(2):
    #print(times.iloc[i,:].max()) #363.0, 411.0
    years.append(list( times.iloc[i,:] ))
#%%
years=[[88.0, 35.0, 39.0, 230.0, 363.0, 224.0, 260.0, 265.0, 310.0, 270.0, 300.0, 200.0],
[109.0, 34.0, 48.0, 305.0, 411.0, 274.0, 274.0, 256.0, 381.0, 408.0, 277.0, 251.0]]

x=np.arange(1,12,1)
plt.figure(figsize = (6,4), facecolor='lightgreen')
plt.rcParams['font.family']='Microsoft Yahei'
plt.title("yearly and times total",size=24)
plt.xlabel("時段",size=16)
plt.ylabel("各年總計",size=16)

x = ["0-2", "2-4", "4-6", "6-8",'8-10','10-12','12-14','14-16','16-18','18-20','20-22','22-24' ]
plt.plot(x,years[0],'bo--',label='99',linewidth=1, markersize=4)
plt.plot(x,years[1],'ro--',label='100',linewidth=1, markersize=4)  
plt.legend(title='年份',title_fontsize=16,bbox_to_anchor=(1.2, 1),loc='upper right', shadow=True) 
plt.xticks(rotation=45, ha='right')

plt.show() #test-1_plot.png

#%%
rt=pd.DataFrame(y1.iloc[:,[0,1,-1]])
print(rt)
print(rt.groupby('organ').sum())
#中和第二分局      2574
#土城分局        3038
print(rt.groupby('year').sum())
#100年      3028
#99年       2584


#todo format output and output to csv
#pandas.DataFrame.to_csv
#https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.to_csv.html

#plt.axis([0,8,0,70])
#print(df.groupby('year').sum())
#print(df.groupby(['organ','year']).sum())
