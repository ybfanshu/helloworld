import numpy as np
import pandas as pd

df=pd.read_csv('每年新北市A2交通事故時間別.csv',header=0,encoding='UTF-8')
'''
y1=df.head(17)


#print(y1)

#y1[y1.columns[2:]]=y1[y1.columns[2:]].astype(float)
print(y1.dtypes)
print(len(y1.columns)) #14 columns
print(y1.iloc[0,2:])
#https://blog.csdn.net/Junbin_H/article/details/68066143
'''


rowTotal=[]
for i in df.index:
    #print(y1.iloc[[i],[0,1]])
    n=df.iloc[i,2:]
    rowTotal.append(int(n.sum()))
df['rowTotal']=rowTotal #add column 小計
#print(df.iloc[ :, [0,1,-1] ]) #display year, organ, rowtotal
df2=df.iloc[ :, [0,1,-1] ]

import csv
df2.to_csv('A2_rowtotal.csv',index=0, quotechar='"', quoting=csv.QUOTE_NONNUMERIC, encoding='utf-8')
#https://stackoverflow.com/questions/16923281/writing-a-pandas-dataframe-to-csv-file
#https://stackoverflow.com/questions/41960346/how-to-add-quotes-to-every-element-in-non-numeric-column-of-a-dataframe-save

#%%
#find top 3 of  year 99 and 104 
import pandas as pd
rt=pd.read_csv('A2_rowtotal.csv',header=0,encoding='UTF-8')
#g=rt.groupby('year',sort=False).count().index
mask = (rt['year'] =='99年') #== g[0]
y99=rt[mask]
#lst = sorted(y99, key=lambda x: x[2], reverse=True)
#print(lst.head(3))
print(y99.sort_values('rowTotal',ascending=False).head(3).to_string(index=False))

mask = (rt['year'] =='104年') #== g[-1]
y104=rt[mask]
print(y104.sort_values('rowTotal',ascending=False).head(3).to_string(index=False))
#screenshot top3-yr99-104.png

#https://sparkbyexamples.com/pandas/print-pandas-dataframe-without-index/
#%%
#find yr 99 and 100 total diff, yearly-growth.png
import pandas as pd
rt=pd.read_csv('A2_rowtotal.csv',header=0,encoding='UTF-8')
def ytotal(y):
    year=str(y)+'年'
    mask = (rt['year'] == year)
    ylist=rt[mask]
    return ylist['rowTotal'].sum()

print()
for i in range(99,104):
    x=ytotal(i)
    y=ytotal(i+1)
    r=(y-x)/x*100
    print('%d年: %d件, %d年: %d件, 成長: %+.2f%%'%(i,x,(i+1),y,r))

'''
x=ytotal(99)
y=ytotal(100)
print('%+.2f'%((y-x)/x*100))
x=ytotal(100)
y=ytotal(101)
print('%+.2f'%((y-x)/x*100))
'''

#https://www.bbc.co.uk/bitesize/guides/zpjmjty/revision/2
#percentage increase = increase ÷ original number × 100
#%%
import pandas as pd
rt=pd.read_csv('A2_rowtotal.csv',header=0,encoding='UTF-8')
print()
#ot=['新莊分局','三重分局','蘆洲分局','土城分局','瑞芳分局']
rt['organ'] = rt['organ'].replace(['中和分局'], '中和第一分局') #y99 中和分局 > 中和第一分局
ot=list(rt.groupby('organ',sort=False).count().index)

a=[]
for i in ot:
    mask = (rt['organ'] == i)
    a.append(list(rt[mask].iloc[:,2]))
#print(a)
#[[2330, 1588, 2160, 3165, 4528, 4683], [1696, 1602, 2079, 2189, 2693, 3014], [1914, 1859, 1965, 2610, 3375, 2993], [1299, 1739, 1938, 1844, 2062, 1717], [114, 280, 250, 307, 403, 406]]
#print(a[1][1]) #1602

#https://www.geeksforgeeks.org/different-ways-to-create-pandas-dataframe/
data=[]

for o in range(len(ot)):
    x,y,z=a[o][0],a[o][1],a[o][2]
    #r= ( '%+.2f' %((y-x)/x*100) )
    r=((y-x)/x)*100
    r2=((z-y)/y)*100
    data.append([ot[o], x, y, z, round(r,2), round(r2,2)])
#print(data)
ydiff = pd.DataFrame(data, columns=['Organ', '99年','100年','101年','成長 %', '成長2 %'])
#print(ydiff)

print(ydiff.sort_values('成長 %',ascending=False).to_string(index=False))
print('-'*37)
#print(ydiff.sort_values('成長2 %',ascending=False).to_string(index=False))
#yr99-100-growth.png
#todo screenshot console?
#==

print('\n\n')
#yr 99 and 100 growth %
for o in range(len(ot)):
    #print(f"{ot[o]}:")
    yr=99;
    yr2=100;
    x,y=a[o][0],a[o][1]
    r=(y-x)/x*100
    print('%s - %d年: %d件, %d年: %d件, 成長: %+.2f%%,\n' % (ot[o], (yr),x,(yr2),y,r))

#yearly growth %
for o in range(len(ot)):
    print(f"{ot[o]}:")
    yr=99;
    yr2=100;    
    print('%d年: %d件'% (yr,a[o][0]))
    for i in range(5):
        x,y=a[o][i],a[o][i+1]
        r=(y-x)/x*100
        print('%d年: %d件, 成長: %+.2f%%' % ((yr2+i),y,r))
    print('~'*37+'\n')


#%%    
ot='新莊分局'
mask = (rt['organ'] == ot)
#print(rt[mask].iloc[:,2])
a=list(rt[mask].iloc[:,2])
#print(a)
#[2330, 1588, 2160, 3165, 4528, 4683]

yr=99;
yr2=100;
for i in range(5):
    x,y=a[i],a[i+1]
    r=(y-x)/x*100
    print('%d年: %d件, %d年: %d件, 成長: %+.2f%%' % ((yr+i),x,(yr2+i),y,r))
    
#%%
import pandas as pd
import matplotlib.pyplot as plt

rt=pd.read_csv('A2_rowtotal.csv',header=0,encoding='UTF-8')
print(rt.head(3))
print(rt.dtypes)
'''
mask = (rt['organ'] =='土城分局')
branch=rt[mask]
organ=list(branch.iloc[:,2])
print()
x=np.arange(99,105,1)
plt.plot(x,organ,'bo--',label='土城',linewidth=1, markersize=4)
plt.show()

#https://stackoverflow.com/questions/33440640/python-pandas-pandas-core-groupby-dataframegroupby-object-at
'''
#before and after console screenshot, rt_organ-replace- png
print(rt.groupby('organ',sort=False).count())
rt['organ'] = rt['organ'].replace(['中和分局'], '中和第一分局') #y99 中和分局 > 中和第一分局
#print(df.drop('Charlie', axis=0))
print(rt.groupby('organ',sort=False).count()) #6 rows per organ

g=rt.groupby('organ',sort=False).count().index
print(g,'\n', g.size,'\n') #16

rt_col=rt.iloc[:,2]
#print(rt_col.dtypes)
print('max: ',max(rt_col),'\nmin: ',min(rt_col),'\nmean: %.0f \n' %(sum(rt_col)/len(rt_col)))
#max:  4683 新莊104年, min:  94 金山99年
# mean:  1566


organ=[]
x=np.arange(99,105,1)
styles=['o-', 's--', 'o-.', 'h--']
count=0;
beingsaved = plt.figure(figsize = (6,4),dpi=300)
for i in range(g.size):
    #print(g[i])
    mask = (rt['organ'] == g[i])
    branch=rt[mask]
    organ.append(list(branch.iloc[:,2]))
    plt.plot(x,organ[i],styles[count],label=g[i][:-2],linewidth=1, markersize=4)
    count+=1
    if count>3: count=0
#print(len(organ))
plt.rcParams['font.family']='Microsoft Yahei'
plt.title("分局各年總計", pad=20,size=24)
plt.xlabel("年分",size=16)
plt.ylabel("數\n量",size=16,rotation=0,labelpad=20)
plt.legend(title='分局',title_fontsize=16,bbox_to_anchor=(1.3, 1.2),loc='upper right', shadow=True) 
plt.ylim(0,5000)
#beingsaved.savefig('destination_path.svg', format='svg', dpi=1200)
plt.show() #branches_rt_plot.png

#https://stackoverflow.com/questions/74576966/printing-name-of-column-after-group-by-in-pandas
#https://datatofish.com/replace-values-pandas-dataframe/
#https://note.nkmk.me/en/python-pandas-drop/
#https://www.geeksforgeeks.org/python-find-maximum-value-in-each-sublist/
#https://stackoverflow.com/questions/16183462/saving-images-in-python-at-a-very-high-quality
