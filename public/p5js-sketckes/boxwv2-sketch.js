//Base on cubes waves, codetrain #86
//https://editor.p5js.org/jhedberg/sketches/rk8ydh6s7
var mult = 85.0;
var heightMult =68.0;
var mult2 = 0.0;
var heightMult2 =0.0;
var mm1 =true;

let blocks=[];
let angle=0;
let w=24;
var ma,maxD,wv,hv;

var isPause=false;
var sss=[]; //slider array
var ns=1; //# of sliders
var sd=[]; //slider default
var allv=[]; //sliders values
var bxs =[]; //checkboxes

function setup(){
   frameRate(10); createCanvas(600,300,WEBGL); 
   ma = atan(1/sqrt(2));
   wv = width/2; hv = height/2;
   setSliders();//drawSliders(); 
   maxD=dist(0,0,wv,hv);
   createSpan('q,s,mouse-position : soft-reset, (un)stop, direction').style('color','#fff').position(4,4);
   setCheckbox();
   setButtons();
}

function mouseMoved() {//85,68
   //if (mm1=== true){
    mult = map(mouseX, 0, width, 0, 85*2);
    heightMult = map(mouseY, 0, height, 0, 68*2);
   /*} else {
     mult2 = map(mouseX, 0, width, -1, 1);
       heightMult2 = map(mouseY, 0, height, -2, 1);
        };*/
   //mult2=mouseX-wv; 
}

function draw(){
    background(0, 150, 200);
    drawSliders();
    ortho(-wv, wv, hv, -hv, 0, 500);
    if (bxs[0].checked()){n=-1} else {n=1} //upside down
    rotateX(n*ma)
    rotateY(-QUARTER_PI);
    translate(200,0,60);//width/height?
    
    //stroke(255);//fill(0);
    noStroke();
    //normalMaterial();
    if ( mult > 85){mult2=wv} else {mult2=-wv};
    directionalLight(242, 150, 92,mult2,0,40);
    directionalLight(31, 80, 81,27.5,-232,-30);
    //print(mult);
    if (bxs[2].checked()){ bxs[1].checked(true); bxs[1].mouseClicked(znot);}
    
    let gap = 10;
   for (let z=0; z < height/2; z+= w+gap){ //blocks length
       for (let x=0; x< (width+gap)/3; x+= w+gap){
           push();
           let d = dist(x,z,mult,heightMult);//85,68);// wv,hv);
           let offset = map(d, 0, maxD, q0,-q0);//-q2,q2);
           let a = angle+offset;
           let h = map(sin(a), -1, 1, 0, 155); //block height
           translate(x - wv + gap, 0, z-hv);
           
           //cube rotation
           if(bxs[1].checked()){ 
                rotateX(sin(a/2));
                if(bxs[2].checked()){ rotateZ(-sin(a))
                                    } else { rotateZ(sin(a))}}//angle/5);
           //rotateY stationary
         
           box(w-2, h, w-2);
           pop();
       }
   } 
    angle +=0.2;
}

//misc
function keyPressed() {
   if (keyIsDown(83)||keyIsDown(13)) {//s or enter
       btnf1()};
   if (keyIsDown(81)) {//q; 
       btnf2();sss[0].value(4);sss[0].elt.focus();};
   //if (keyIsDown(65)) {//a; 
    //if (mm1 === true){ mm1=false;}else{mm1=true;}}
}

function setSliders(){
   var svt=createSpan('this').style('color','#fff');
   svt.id('svt');
   
   var si=[-8,8,1];//slider min,max,step
   sd=[4,0,4]; //default values
   var vw ="120px";
  for (var i=0;i<ns; ++i){
     sss[i] =  createSlider(si[0],si[1],sd[i],si[2]);
     sss[i].style("width",vw);
     sss[i].position(30,hv-40*(i+1));//(30,height-(10*i));
  }
}
function drawSliders(){
   fill(255);
   for (let i=0;i<ns;++i){ window["q"+i] = sss[i].value(); 
                            stroke(3);strokeWeight(1);
                         }
   //print sliders values
   let spantxt=select('#svt'); 
   /*let mr=round(mult);let yr=round(mouseY);
    spantxt.html(mr+', '+yr+', '+q2).size(30,30);*/
   spantxt.html(newTxt()).size(30,30);
};

function newTxt(){
   for (var i=0;i<ns;++i){ allv[i]=window["q"+i]};
   let vp=join(allv,', ');
   return vp;// all sliders values
}

function setButtons(){
  var btn1 = createButton('stop');
  btn1.mouseClicked(btnf1).position(30,height-15);//(30,hv-40);
  var btn2 = createButton('mouse reset');
  btn2.mouseClicked(btnf2).position(btn1.x+btn1.width+10,btn1.y);
  /*btn2.id('btn2');*/
  var gotoFirstTab =createButton('gotoSlider');
  gotoFirstTab.mouseClicked(gotoSlider).position(btn1.x,btn1.y+40);
}

function btnf1(){
    if (isPause === false){
          isPause=true;noLoop();
    }else{ loop();isPause=false;};}
function btnf2(){
    mult = 85.0
    heightMult =68.0;
}//location.reload();}  //q0

function setCheckbox(){
   let ack = 4;
   var ckTag=['upside down','rotateX,Z', '-rotateZ','checkAll'];
   for (var i=0;i<ack;++i){
      bxs[i] = createCheckbox(ckTag[i], false).style('color','#fff').position(30,hv+(30*i));
      //bxs[i].changed(checkAct);
   }
   bxs[3].changed(allcheck);
   bxs[1].checked(true);
}

function allcheck(){
   //bxs.length-1
   let ack = 4;
     if (this.checked()) {
           for (let i=0;i<ack-1;++i){ bxs[i].checked(true);}
     } else {
          for (let i=0;i<ack-1;++i){ bxs[i].checked(false);}}
}

function znot(){bxs[2].checked(false);}

function gotoSlider(){sss[0].elt.focus();};