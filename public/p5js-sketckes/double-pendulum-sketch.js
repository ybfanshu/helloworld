//ref
// Daniel Shiffman http://patreon.com/codingtrain
// Double Pendulum https://youtu.be/uWzPe_S-RVE
// p5 spring anim https://www.youtube.com/watch?v=VWfXiSUDquw
// Bouncing DVD Logo

let r1 = 125;
let r2 = 125;
let m1 = 10;
let m2 = 10;
let a1 = 0;
let a2 = 0;
let a1_v = 0;
let a2_v = 0;
let g = 1;

let px2 = -1;
let py2 = -1;
let cx, cy;

let buffer;

var isPause;
var mousePos, target, pos,vel,drag,st
var md,mr,mm,cu
let tnum=6,rot,ww,hh,trc;
for(var i=1;i<4;++i){ window["z"+i] = []};
var fps=18
let pcr,pcg,pcb,bx,by, bxspeed = 5,byspeed = bxspeed;
let isBs=false,islow=0

function setup() {
  createCanvas(500, 300*1.5);
  frameRate(fps)
  rot=0; ww=width/2; hh=height/2;

  //pixelDensity(3);
  a1 = PI / 2 ;
  a2 = (PI / 2) + (floor(random(-1,2)) * random(hh));
  console.log(a2)
  cx = width / 2 ;
  cy = 50;
  buffer = createGraphics(width, hh);
  buffer.background(175);
  buffer.translate(cx, cy);
  buffer.text('enter or click to (un)pause',0,0)
  buffer.text('\\ or mouseDrag to clear bg',0,15)
  
	mousePos = createVector(0,0);
	pos = createVector(0,0);
	target = createVector(0,0);
	vel = createVector(0,0);
	drag=0.5;
	st=0.2; //strength
	md = 30; //diameter
	mr = md / 2;
	mm = mr * 1.4;
	cu=0; //circle up/down
	trc=(md/tnum)
	
	pickColor()
	bx = random(width);
	by = random(height);
	isPause=false;
	print(isPause)
}

function draw() {
  //background(175);
  translate(0,100)
  imageMode(CORNER);
  image(buffer, 0, 0, width, hh);

  let num1 = -g * (2 * m1 + m2) * sin(a1);
  let num2 = -m2 * g * sin(a1 - 2 * a2);
  let num3 = -2 * sin(a1 - a2) * m2;
  let num4 = a2_v * a2_v * r2 + a1_v * a1_v * r1 * cos(a1 - a2);
  let den = r1 * (2 * m1 + m2 - m2 * cos(2 * a1 - 2 * a2));
  let a1_a = (num1 + num2 + num3 * num4) / den;

  num1 = 2 * sin(a1 - a2);
  num2 = (a1_v * a1_v * r1 * (m1 + m2));
  num3 = g * (m1 + m2) * cos(a1);
  num4 = a2_v * a2_v * r2 * m2 * cos(a1 - a2);
  den = r2 * (2 * m1 + m2 - m2 * cos(2 * a1 - 2 * a2));
  let a2_a = (num1 * (num2 + num3 + num4)) / den;

  translate(cx, cy);
  stroke(0);
  strokeWeight(2);

  let x1 = r1 * sin(a1);
  let y1 = r1 * cos(a1);

  let x2 = x1 + r2 * sin(a2);
  let y2 = y1 + r2 * cos(a2);

  line(0, 0, x1, y1);
  fill(0);
  ellipse(x1, y1, m1, m1);

  line(x1, y1, x2, y2);
  fill(0);
  ellipse(x2, y2, m2, m2);

  a1_v += a1_a;
  a2_v += a2_a;
  a1 += a1_v;
  a2 += a2_v;
  /*push()
  fill(255)
  ellipse(px2,py2,30)
  pop()*/
  // a1_v *= 0.99;
  // a2_v *= 0.99;

  //buffer.stroke(1); //color
  let bsw=abs(a2)
  let bsw2=bsw*2 
  //175
  if (bsw > 12|| bsw2 >12){bsw %=2; buffer.stroke(255);} 
   else {buffer.stroke(1); bsw = bsw2;}
  buffer.strokeWeight(bsw);
  if (frameCount > 1) {
  	//buffer.translate(-cx, -cy);
    buffer.line(px2, py2, x2, y2);
    //buffer.point(x2,y2)
  }
  //*/

  px2 = x2;
  py2 = y2;
  
  //
  ///*
  mousePos.set(px2, py2);
	target = mousePos;
	var force = p5.Vector.sub(target, pos);
	force = force.mult(st);//+(5*bsw)
	vel = vel.mult(drag);
	vel = vel.add(force);  
	pos = pos.add(vel);
//*/	
  rot +=.5 //0-1 rotate speed	
  trail(pos.x, pos.y, rot)
//trail(px2,py2,rot)
        push()
        translate(x2,y2)
  fill('red')
  rotate(rot);
  ellipse(0,-md,md)
  ellipse(mm,mm,mr)
  pop()
  //print(y2)
  //if (rot % 100 ==0){
  switch (rot){
  case 100:
  case 200:
  case 300:
   if (y2 > 214){islow+=1}
   //if (y2 < 205 && islow !==0){ islow-=1}
   if (islow == 2){ a2new();islow=0}
   //if (islow !== 0){ islow-=1}
   //if (islow==3){};break
   break;
  case 360:
   rot=0;isBs=!isBs;break
  }
  //if (rot==360){rot=0;isBs=!isBs;}
  //print(rot,isBs)
  
  push()
  
  switch(isBs){
  case true:
  rotate(-rot);
  break;
  case false:
  break;}
  translate(-cx,-cy-100)
  //ot=rect(mouseX,mouseY,80,60-(5*bsw))
  strokeWeight(bsw)//3
  bgbounce()//(5*bsw))
  //console.log(bx)
  pop()
  //console.log('x: '+x2,'y: '+y2)
}

function a2new(){
	let t=isFinite(a2)
	switch (t){
	case true:
	break;
	case false:
	location.reload();
	break;
	}
//	a1=random(-1,2)*random(3)
  a1 = (floor(random(-1,2))) * PI / 2 ;
  a2 = (PI / 2) + (floor(random(-1,2)) * random(hh));

}

function trail( tra0,tra2,tra3 ){
	let which = frameCount % tnum;  
	z1[which] = tra0
	z2[which] = tra2
	z3[which]= tra3
	let tr = trc;
    fill(0)
	for (let i=0;i<tnum; ++i){	
	 let ix = (which+i+1)%tnum;
	 tr += trc
	 tr *=0.7
     push()
     translate(z1[ix],z2[ix])
     rotate(z3[ix])
     	fill(0)
  ellipse(0,-md,tr)
		fill(255,255,0)
  ellipse(mm,mm,tr);
  pop()
  }
  
}

function touchStarted(){
btnf2();
}
function mousePressed(){
btnf2();
}
function mouseDragged(){
	btnf3();loop();//btnf2();
}

function btnf2(){
//print(isPause)
switch (isPause) {
case true:
loop();isPause=!isPause;
break;
case false:
isPause=!isPause;noLoop();
break;
}
}
function btnf3(){
	buffer.background(175);
 	background(0);
 	a2new()
}

function pickColor(){
  pcr = random(100, 256);
  pcg = random(100, 256);
  pcb = random(100, 256);
}

function bgbounce(bh2){
//push()
//rotate(-rot)
//translate(-cx,-cy-100)
  let bw=80;bh=60
  fill(pcr, pcg, pcb);
  //bh -= bh2
  rect(bx,by, bw,bh)
  bx += bxspeed;
  by += byspeed;
  
if (bx + bw >= width) {
    bxspeed *= -1;
    bx = width - bw;
    pickColor();
  } else if (bx <= 0) {
    bxspeed *= -1;
    bx = 0;
    pickColor();
  }
  
  if (by + bh >= height) {
    byspeed *= -1;
    by = height - bh;
    pickColor();
  } else if (by <= 0) {
    byspeed *= -1;
    by = 0;
    pickColor();
  }
//pop()
}

function keyPressed() {
 if (keyIsDown(13)) {//enter to (un)pause
	print(mouseX,mouseY)
	print(a1,a2,islow,rot)
	btnf2()
	//console.log(randPic, next,picUrls.length);
 }
 if (keyIsDown(220)) {
 	btnf3()
 }
}
