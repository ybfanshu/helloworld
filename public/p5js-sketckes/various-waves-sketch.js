//to-do : make colorlist?, checkbox to use default slider values v1-3 for each pattern. add func for curveVertex or point.
//array of function in slider ? 
//https://stackoverflow.com/questions/28186880/how-to-store-functions-into-an-array-and-loop-through-each-in-javascript
	//https://stackoverflow.com/questions/49552862/is-there-a-way-to-call-all-functions-inside-an-object-in-javascript
	//https://www.geeksforgeeks.org/object-values-javascript/
//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/getOwnPropertyNames
let xspace=16;
let w,range;
let theta=0;
let amp=75;
let period=30;
//let dx;
var isPause=false;
var cnt=0;
let angle=0;
let startAngle = 0;
let angleVel = 0.4;
var hw,hh;

var sss=[]; //slider array
var tx=['st1','st2','st3']; //slider text
//var sd=[3,25,42,1,1,5]; //slider default
var sd=[23,45,12,1,1,5]; //slider default
var allv=[]; //sliders values
var cv=0; //checked n slider, sss[cv].value

let radio,rval, keyRadio=0, getRB=1;
let rbW=20,rbH=100;
let infospan, infoshow=false;
let previousSlider='0';
var brightness=false; // for wave color

var ptn; //pattern slider
var poc=[]; //pattern function array
let isVtx=false; //is vertex or point

function setup(){
 setInfo()
 createCanvas(700,400);
 hh=(height/2)
 hw=(width/2)
 frameRate(10)
 setSlide()
 setRadioBtn()
 
 //to-do move below to setslide and such, count poc amount minus one = max
 let zx=8
 ptn=createSlider(0,zx,zx) //slider (min,max,start at)
 ptn.position(width-180-25,120);
 //ptn.value(1)
 poc=Object.getOwnPropertyNames(patterns)
 print(poc.length) //-1
 ptn.changed(ptnfn)
}

//= draw =//
function draw(){
 background(33);
 drawSlide();
 drawRB();
 drawRadio();
 //push();
  fill(255);noStroke()
  xspace = sss[cv].value();
  //movingSine();
  ptnfn();
  //pop()
}

function ptnfn(){
  //patterns.movingSine()
  //patterns[poc[0]]()
  patterns[poc[ptn.value()]]()
}

var patterns = {

//// ==sine
 movingSine: function(){
 switch (cnt){
  case 64:
	startAngle=0.07;
	cnt=0
  break;}

 startAngle += .1 //0.015;
 angle = startAngle;
 cnt += 1
 
 translate(0,hh/2)
 noStroke();fill(255);
 for (let x = 0; x <= width; x += xspace) {
	y = map(sin(angle),-1,1,0,hh);
	ellipse(x,y,25,25);
	angle += angleVel;
 } 
},
//to-remove//// ==Double sine
 dSine: function(){
 //test 03-2, double sine i+=3, single sine i+=6?
 switch (cnt){
  case 32:
	startAngle=0.07;
	cnt=0
  break;}

 cnt +=1
 
 translate(width/4,hh/2)
 stroke('red');strokeWeight(5);noFill()

 startAngle += .1 //.1, 0.015, 2;
 angle = startAngle;

 beginShape();
  for (let i=0;i<=width/2;i+=xspace ){ //i+=3 , 6
   	ta=i+angle
   	amp=.4
   	let s1 = sin(ta)*amp
    y = map(cos(ta)*amp,-1,1,0,hh);
   	point(s1+i,y)
  }
 endShape();
},
//// ==helix
helix: function(){
 switch (cnt){
  case 32:
	startAngle=0.07;
	cnt=0
  break;}
 cnt +=1
 
 translate(hw/2,hh)
 stroke('red');strokeWeight(5);noFill()

 startAngle += .1 //.1, 0.015, 2;
 angle = startAngle;

 /* //// ==helix, slider value at 39
 amp=40; range=xspace*.15
 beginShape();
  for (let i=0;i<=hw;i+=range){ 
   	ta=i+angle
 */
 /* //// ==faster helix, slider at 29
 amp=40
 range=xspace*.2
 //sss[0].value(31)
 beginShape();
  for (let i=0;i<=hw+range;i+=range){
   	ta=i+angle*range
 */
 /* //// ==cloth wave like, slider at 19
 ^^ startAngle += .66
 ....
 amp=40
 beginShape();
  for (let i=0;i<=hw;i+=xspace){
   	ta=i+angle
 */
 //// ==helix pattern 2, 15
 amp=40 // 40/(xspace*.2); //i<=hw;i=6
 range=xspace*.2
 sss[0].value(15) //29
 beginShape();
  for (let i=0;i<=hw;i+=range){ //i+=3, 6, 12, 13.5, 20, 28.5
   	ta=i+angle
   	let u=0.5 //test, remove?
   	let s1 = sin(ta/u)*amp
   	let c1 = cos(ta/u)*amp
   	//point(s1+i,c1)
   	point(c1+i,s1)
  }
 endShape();
},
//// ==not helix
 test3c: function(){
 //test 03-3
  switch (cnt){
  case 32:
	startAngle=0.07;
	cnt=0
  break;}
 cnt +=1
 
 translate(hw,hh)
 stroke('red');strokeWeight(5);noFill()

 startAngle += .2 //.1, 0.015, 2;
 angle = startAngle;

 //xspace=15
 range=xspace*.2
 amp=.4
 beginShape();
 for (let i=-hh;i<=hh;i+=range){ //remove
  //for (let i=-hh;i<=0;i+=range){ //i+=3 , 2.5
   	/*ta=(i*angle)/(38.5/range) //v0=38.5, i+=3 //// ==m curves
   		let s1 = sin(ta)*amp
   		let y = map(sin(ta),-1,1,0,hh);
   			//point(s1+i,y) 
   		point(s1+i,s1*y)
   	*/
   	ta=i+angle //i+2.5*angle, ^^;i+=2.5 //// ==4 legs spiral 
   	let c1 = cos(ta)*amp
   	let s1 = sin(ta)*amp
	   	//point(s1+i,c1*i)
	   	curveVertex(s1+i,c1*i)
   	//*/
   	
  }
 endShape();
},
//// ==wiggle
 wiggle1: function(){
 switch (cnt){
  case 20:
	angle=0.0;
	cnt=0;
  break;}

 cnt +=1;
 
 translate(hw,hh)
 stroke(255);strokeWeight(16)
 noFill()
 beginShape();
 //let range=8 /*//+15, //r2 +35, //r3 +1, +44*/
 range=hw-xspace
 for (let i=-range;i<range;i+=xspace){ 
 //15, //r2 33, //r3 40, 2.5
	let r=sin(angle)+5 //r3 fat worm
	point(i,r)
	angle+=.4
 }
 endShape();
},
//// ==cloverleaf
slowBounces: function(){ 
 switch (cnt){
  case 52:
	angle=0.0;
	cnt=0;
  break;}
 cnt +=1;
 
 translate(175,hh)
 stroke(255);strokeWeight(16)
 fill(233);noFill()
 line(2,hw)
 range=12*PI*5 //*1, *4.9
 beginShape();
 for (let i=0;i<range;i+=xspace){ //5, 25, 25.4
  let r=(sin(2*angle)+1/4*sin(6*angle))*i
  vertex(i,r)
  angle+=.4
 }
 endShape()
},
//// ==straight tail
tailwave: function(){
	manySine(12.16)
},
//// ==double rotating sine
rotatingSine: function(){
	manySine(2.8)
},
//// ==testing
miscSine: function(){
	manySine(.4*v2)
}
//end pattern
};

function manySine(man){
 switch (cnt){
  case 30:
	angle=0.0; cnt=0; break;}
 cnt +=1;

 translate(hw,hh); stroke(255);strokeWeight(16);noFill()
 range=3+xspace //6, 3+xspace
 beginShape();
 for (let i=-range;i<range;i+=.4){ 
	let a=7.3
	let pi=3.1
	let amp=22
	let rx=(i*cos(pi*a/2)+sin(pi*a/2)*cos(pi*(i-1/2)))*amp
	let ry=(i*sin(pi*a)+1/2*sin(pi*a/2)*sin(2*pi*(i-1/2)))*amp
	rx=ry*sin(i+angle)
	point(rx,ry)
	angle+=man //12.16, 2.8, 0.4*xspace, 12.4, 17.2, 1.2
 }
 endShape()
}


//== ui ==//

function btnf1(){
	if (isPause === false){
	  isPause=true;noLoop();
	  }else{ loop();isPause=false }
}
function btnf2(){
	switch (infoshow){
	case false: infospan.show(); infoshow=true;break;
	case true: infospan.hide(); infoshow=false;break;
	}
}
function btnf3(k){
    keyRadio=String(int(k)-1)
    //let skr=String(keyRadio)
	focusRadio(keyRadio)
    //print(k)
	btnf4();
}
function btnf4(){
//when switch from radioBtn to slider focus on selected radioBtn value
	switch (getRB){
	 case 0: 
	 	sss[keyRadio].elt.focus(); 
	 	break;
	 case 1: 
	 	focusRadio(keyRadio);
	 	break;
	}
}

function drawRB(){
	//let rbW=20,rbH=100;	
	switch (getRB){
	 case 0: text('choose slider(s)',rbW,rbH);break;;
	 case 1: text('choose radioBtn(s)',rbW,rbH);break;;
	}
	switch (isPause){
	 case true:
	 push();rect(0, rbH-15, 70,20);fill(0);text('pause',rbW,rbH);pop();
	 break;
	}
}

function keyPressed() {
 switch (key){
 	case '0':
 		btnf2(); //show info
 		break;
 	case '1': case '2': case '3':
 	//case 'C': case 'c':
		//print(key)
		btnf3(key);
 	break;
 	case '4':
	 	ptn.elt.focus(); 
 	break;
 	case 's': case 'S':
 		getRB ^= true;
 		btnf4();
 		//print(getRB)
 	break;
 	case 'q': case 'Q':
 		btnf1()
		print('pause/play');
	break;
	case 'r': case 'R':
		window.location.reload();
	break;
 }
}

function setInfo(){
 infospan=createSpan('R:refresh page, <br>Q:(un)stop,<br>1-3:radio button,<br>4 slider, 0:info toggle').style('color','#fff').style('width','25rem').style('position','fixed')
 createButton('inf0&#818').position(0,height).mouseClicked(btnf2);
 infospan.hide()
}

// https://p5js.org/reference/#/p5/createRadio
function setRadioBtn(){
 getRB = 1;
 radio = createRadio();
 radio.option('0');
 radio.option('1')
 radio.option('2')
 radio.style('font-size','20px').style('width', '45px').position(width-10,35);
 radio.value('0')
}

function drawRadio(){
 cv = int(radio.value()) //get selected radio value for slider array
 //https://stackoverflow.com/questions/2652319/how-do-you-check-that-a-number-is-nan-in-javascript
}

function focusRadio(a){
 //if (brightness === true && a === '2'){return} //3rd slider for color
 radio.value(a) //set radio to number a, string format
 select('#defaultradio0-'+a).elt.focus()
}

function slideclick(a){
 if (previousSlider !== a){
 //print(b,a)
 radio.value(String(a))
 }
 previousSlider=a
}

function setSlide(){
var vw ="180px";
 for (var i=0;i<3; ++i){
  sss[i] =  createSlider(1,50,sd[i],2); //.4,50,sd[i],.5);
  sss[i].style("width",vw);
  sss[i].position(width-180-25,20*i+40);
  sss[i].attribute('onClick','slideclick('+i+')');
 }
 textSize(15);
}

function drawSlide(){
 fill(255);
 for (var i=0;i<3;++i){ window["v"+i] = sss[i].value(); 
  stroke(3);strokeWeight(1);
  //text(tx[i]+': '+window["v"+i],20,20*i+40);
  text(window["v"+i],20,20*i+40);
 }
 text(ptn.value(),width-25,120);
}