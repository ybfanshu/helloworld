var t=0; //speed
var x1,y1,x2,y2; 
var sss=[]; //slider array
var ns=6; //6 sliders
var tc=255,tf=1; //text color

let num=6; //trail lines
for(var i=1;i<5;++i){ window["z"+i] = []};
var tx=['c1','s2','c2','x2*','speed','stroke']; //slider text
var sd=[10,5,12,1,1,5]; //slider default
sd=[5,3,6,4,1,10]; 
sd=[9,10,15,4,2,15];
var allv=[]; //sliders values

function setup(){
   frameRate(15);
    createCanvas(700,400); 
  
  var vw ="180px";
  for (var i=0;i<ns; ++i){
     sss[i] =  createSlider(1,50,sd[i],1);
     sss[i].style("width",vw);
     //sss[i].position(width-180-25,20*i+40);
	sss[i].position(windowWidth*.6,20*i+70);
  }
  textSize(15);
}

function draw(){
   background(0);
   fill(tc);
   for (var i=0;i<ns;++i){ window["v"+i] = sss[i].value(); 
                           stroke(3);strokeWeight(1);
                           text(tx[i]+': '+window["v"+i],20,20*i+40);
                         }
   text ('a/s/q/= : play/stop/reload/toggle', 20,20);
  
   translate(width/2,height/2);
   t+= v4;
 push();
   strokeWeight(v5);
   l3();
   ll(t);
   pop();
}

function ll(t){
  //l3(x1,y1,x2,y2);
    stroke(255);//color
    let s1=sin(t/10)*100;
    let c1=cos(t/v0)*150; // 10
    let s2=sin(t/v1)*20 ; //  5
    let c2=cos(t/v2)*20 ; // 12
  
    x1=s1 + s2;
    y1=c1;
    x2=s1*2 + s2*v3;
    y2=c1*2 + c2;
    line(x1,y1,x2,y2)
}

function l3(){
  
    let which = frameCount % num;  
    z1[which] = x1
    z2[which] = y1
    z3[which] = x2
    z4[which] = y2
    
    for (let i=0;i<num; ++i){
        let ix = (which+i+1)%num;
          //print(which, ix);
        stroke(35+i*20);
        line(z1[ix],z2[ix],z3[ix],z4[ix]);
}}

function keyPressed() {
     if (keyIsDown(65)) {//a;RIGHT_ARROW
           loop();}
    if (keyIsDown(83)) {//s;LEFT_ARROW
       for (var i=0;i<ns;++i){ allv[i]=window["v"+i];}
          let vp=join(allv,', ');print(vp);noLoop();}
    if (keyIsDown(81)) {//q; 
          //location.reload();}
          for (var i=0;i<ns; ++i){sss[i].value(sd[i]);}}
    if (keyIsDown(187)) {//=;
        if (tf === 1){
             for (var i=0;i<ns; ++i){sss[i].hide()};tf=0;tc=0;
        } else {
             for (var i=0;i<ns; ++i){sss[i].show()};tf=1;tc=255;
        }
    }
}