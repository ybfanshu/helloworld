/*from https://youtu.be/SKDhkB8g1So
--todo: auto switch image w/ var fc2, rename variables
*/
let fps=10;
var isPause =false;
let font;
let input;
var next=1, randPic=0, pickthis=0;
var picUrls=[],pic=[];
let ps,pstxt,ishide,tohide //pic slider
let cl,cm;//color list, menu
let urlbtn, urlok
var fc2=0,watcolor;

function preload() {
  tenImage();
  cl=loadStrings('./colorNames.txt')
}

function setup() {
  frameRate(fps);
  createCanvas(400, 100);
  //textFont(font);
  textSize(45);

  input = createInput('+')//'enter=(un)pause');
  input.position(width/2, height+20);
  //imageMode(CENTER)
  shuffle(cl, true);
  cm= createSelect();
  cm.option('black');
  cm.option('white');
  cm.position(input.x +input.width+ 5,input.y)
  for (let i=1;i<cl.length;i++){
   cm.option(cl[i]);}

  cm.changed(txtInput); //https://p5js.org/reference/#/p5/changed
  cm.input
  //cm=createButton('randomColor').position(input.x +input.width+ 5,input.y).mouseClicked(colorbtn)
  //watcolor=color(0)
  setps(0) //slider to change image
  pstxt= createP('').position(cm.x,ps.y-15)//.hide() //slider value position
  
  //hideinfo=1;
  createButton('info').position(pstxt.x+50,ps.y).mouseClicked(showInfo);
  //keyinfo = createP('back-/=next enter=pause esc=hide').hide()
  createButton('+5').position(pstxt.x+150,ps.y).mouseClicked(btnf3);
  urlbtn=createButton('url').position(pstxt.x+250,ps.y).mouseClicked(btnf4);
  urlbtn.attribute('disabled','disabled')
  urlok=false
  
	//tohide=selectAll('input')
  ishide=0;  
  tohide=document.querySelectorAll("body > :not(canvas)");
  /*for (let z = 0; z < tohide.length; z++) {
	console.log(tohide[z]);
  }*/
  //tohide=['.ps','input','select','p','button'];
  let sp=selectAll('p')
  //https://www.sitepoint.com/shorthand-javascript-techniques/
  for (let p of sp) p.style('color','#fff');
  
  //btnf3()
}

function draw() {
//print(pic.length)
//  background(255);
  pstxt.html(pickthis)
  image(pic[pickthis],0,0);
  txtInput();
  tileWave(5, 9, 0.4,6,13)//7,9,0.2,7,11
}
function colorbtn(){
let r_=random(250)

  let g_=random(250)

  let b_=random(255);
  watcolor=color(r_,g_,b_)
}
//Canvas2D: Multiple readback operations using getImageData are faster with the willReadFrequently attribute set to true. See: https://html.spec.whatwg.org/multipage/canvas.html#concept-canvas-will-read-frequently

function txtInput(){
  push();
    
  let txtcolor = cm.value().toLowerCase();
  fill(txtcolor);
  translate(width/2, height/2);
  textAlign(CENTER, CENTER);
  text(input.value(), 0, 0);
  pop();
}
function tileWave(tx, ty, s1, s2,s3) {
  fc2 +=1 ;
  switch (fc2){
  case 38:
  case 40:
  fc2=0
  break;}

  let tilesX = tx;
  let tilesY = ty;

  let tileW = (width/tilesX);
  let tileH = (height/tilesY);

  for (let y = 0; y < tilesY; y++) {
    for (let x = 0; x < tilesX; x++) {

      // WAVE
      //let wave = int(sin((frameCount + ( x*y )) * 0.05) * 200);
      //let wave =  sin((frameCount + 0.05+ ( x*y ))/6)*5+ cos(frameCount/6);
      //let wave =  sin((frameCount + 0.2+ ( x*y ))/7)*11
      let wfc=(fc2 + s1+ ( x*y ))/s2
      let wave = sin(wfc)*s3
      //print(x,y, fc2, frameCount, wave)

      // SOURCE
      let sx = x*tileW //+wave;
      let sy = y*tileH +wave;
      let sw = tileW;
      let sh = tileH;


      // DESTINATION
      let dx = x*tileW;
      let dy = y*tileH;
      let dw = tileW;
      let dh = tileH;
      
      copy(sx, sy, sw, sh, dx, dy, dw, dh);
 
    }}
}    
function setps(x){
  let pl=(pic.length-1)
  switch(x){
   case 0:
	ps= createSlider(0,pl,0)
	pickthis=0
	break;
   case 1:
    ps= createSlider(0,pl,pl)
    pickthis=pl
	tohide=document.querySelectorAll("body > :not(canvas)");
	break
  }
  ps.position(width/2,input.y-30).addClass('ps')
  ps.input(picSlider);//btnf2
  //ps.attribute('tabIndex','3')
  //document.getElementsByTagName("INPUT")[1].tabIndex = "-1";
}
function showInfo(){
  /*if (hideinfo == 1){ keyinfo.show(); hideinfo=0
  } else {  keyinfo.hide(); hideinfo=1}*/
  alert('\\ or mouse click = next image \nenter = pause \nesc = hide interface')
}

function tenImage() {
 picUrls=['https://source.unsplash.com/collection/190727/400x100',
	'https://source.unsplash.com/random/400x100']
 ///*
  let suf='https://source.unsplash.com/400x100/?';
 let term=['city','water','misc','fire',
 'lightning','electricity','nature','dog']

 //for (let i of term) append(picUrls, suf+i);
 //*/

  for (let i in picUrls){
	//location.reload();
	print(picUrls[i]);
	pic[i]=loadImage(picUrls[i]);
  }
  //print(term.length, pic.length)
}

function keyPressed() {
 if (keyIsDown(13)) {//enter to (un)pause
	btnf1()
	//console.log(randPic, next,picUrls.length);
	}
 if (keyIsDown(220)) {// \ backward slash; 
	btnf2();}
 if (keyIsDown(27)) {//esc; 	
 //if (ishide == 0){
 //console.log(tohide[1]);
 //hide();show()
  switch(ishide){
  case 0:
   for (let th of tohide) th.style.visibility = "hidden";
   ishide=1; break
  case 1:
   for (let th of tohide) th.style.visibility = "visible";
   ishide=0; break
  }
 }
}

function mouseClicked(){
	btnf2();
}
function btnf1(){
	if (isPause === false){
	  isPause=true;noLoop();
	  }else{ loop();isPause=false }
}
function btnf2(){
	redraw();clear();
	notCanvas=['SELECT','INPUT','BUTTON']
	f=document.activeElement.tagName
	for (let th of notCanvas) {if (f.match(th)){ return;break;}}
	//if (document.activeElement.className.match('ps')){return}
	//https://stackoverflow.com/questions/40683289/check-if-active-element-has-particular-class-via-javascript
	next=(ps.value()+1);
	if ( next == pic.length){next=0;}
	pickthis=next
    ps.value(pickthis)
    //let cr=(document.activeElement.className === 'ps')//.match(ps))
    //print(cr)
//	print(randPic);
}
function btnf3(){
///?? https://medium.com/@gloriajulien/code-test-removing-images-p5-js-1fdd651e3a5f
  //remove array > 30 img
  over30=(picUrls.length+1)
  if (over30 > 31){  pickthis=0;ps.value(0); 
  let bn=5;picUrls.splice(0,bn); pic.splice(0,bn)}//31,5
  //fetchUrl().then(console.log)  
  for (let i=0;i<5;i++) {
    setTimeout( fetchUrl,i*4500 );}
    //function timer(){}
   //console.log(x)
}
function btnf4(){
//https://forum.processing.org/two/discussion/8809/link-function
  pv = ps.value()
  purl = picUrls[pv].split('?')[0]
  console.log(ps.value(),purl)
  window.open(purl,'_blank')
}
function picSlider(){
	redraw();
	pickthis=ps.value()
}

//https://javascript.info/async-await
async function fetchUrl(){
  //console.clear();
  
  const response = await fetch('https://source.unsplash.com/random/400x100')
  const url = await response.url
  //await new Promise((resolve, reject) => setTimeout(resolve, 10000));
  //add new url to array
  picUrls.push(url)
  pic.push(loadImage(url))
  console.log(pic.length, url.split('?')[0])
  //slider remove, create
  ps.remove();
  setps(1);
  //urlok=true
  urlbtn.removeAttribute('disabled');
  //print('fin '+pic.length)  
  return url
}
