//https://p5js.org/reference/#/p5/changed

let sel;
let fps=10;
var isPause =false;
let font;
let input;
var next=1, randPic=0, pickthis=0;
var picUrls=[],pic=[];
let ps,pstxt,ishide,tohide //pic slider
let cl,cm;//color list, menu
let urlbtn, urlok
var fc2=0,watcolor;

function preload() {
  cl=loadStrings('./colorNames.txt')
}

function setup() {
  textAlign(CENTER)
     
  input = createInput('+')
  input.position(width/2, height+20);

  shuffle(cl, true);
  cm= createSelect();
  cm.option('black');
  cm.option('white');
  cm.position(input.x +input.width+ 5,input.y)
  for (let i=1;i<cl.length;i++){
   cm.option(cl[i]);}

  cm.changed(txtInput);
  ps= createSlider(0,1,0)
  ps.position(width/2,input.y-30).addClass('ps')
  pstxt= createP('').position(cm.x, ps.y-15)//.hide() //slider value position
  pstxt.html(ps.value())

  createButton('info').position(pstxt.x+50,ps.y).mouseClicked(showInfo);
  
  ishide=0;  
  tohide=document.querySelectorAll("body > :not(canvas)");

   let sp=selectAll('p')
   for (let p of sp) p.style('color','white');
    
}

function txtInput() {
  let item = cm.value().toLowerCase();
  background(200);
  fill(item)
  text("it's a " + item + '!', 50, 50);
}

function showInfo(){
  /*if (hideinfo == 1){ keyinfo.show(); hideinfo=0
  } else {  keyinfo.hide(); hideinfo=1}*/
  alert('\\ or mouse click=next image \nenter=pause \nesc=hide interface')
}