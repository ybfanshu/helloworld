import java.util.Objects;

public class Student {
	private int id;
	private String name;
	private String tel;
	private String addr;
	private int age;
	public Student(int id, String name, String tel, String addr, int age) {
		super();
		this.id = id;
		this.name = name;
		this.tel = tel;
		this.addr = addr;
		this.age = age;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getAddr() {
		return addr;
	}
	public void setAddr(String addr) {
		this.addr = addr;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	@Override
	public int hashCode() {
		return Objects.hash(addr, age, id, name, tel);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		return Objects.equals(addr, other.addr) && age == other.age && id == other.id
				&& Objects.equals(name, other.name) && Objects.equals(tel, other.tel);
	}
	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", tel=" + tel + ", addr=" + addr + ", age=" + age + "]";
	}
	
}
