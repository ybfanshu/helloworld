<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
  </head>
  <body>
  <div id="app">
    {{test}}
        <table align="center">
            <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Tel</th>
            <th>Addr</th>
            <th>Age</th></tr>
            
            <tr v-for="bran in brands">
            <th>{{bran.id}}</th>
            <th>{{bran.name}}</th>
            <th>{{bran.tel}}</th>
            <th>{{bran.addr}}</th>
            <th>{{bran.age}}</th>
            </tr>
        </table>
   </div>  
	<script src="https://cdn.jsdelivr.net/npm/vue@2.7.14/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios@1.1.2/dist/axios.min.js"></script>
    <script>
    new Vue({
        el:"#app",
        data(){
            return{
            	test:"test",
            	brands:[]
          };
        },
        mounted(){
            var _this=this;
            console.log("mounted");
            axios({ //async
            	method:"get",
            	url:"<%=request.getContextPath()%>/servlet"
            }).then(function(resp){
                _this.brands=resp.data;
                console.log(resp.data);
            })
        }
    })
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
  </body>
</html>