<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<% String path=request.getContextPath(); %>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
  </head>
  <body>
<h2>user list</h2>
<!-- http://localhost:8080/Project_ln16/userServlet?status=login -->

<button type='button' onclick="addUser()">add user</button>
<span> | </span><button type='button' onclick="logout()">logout</button>

<span> | </span><!--  <form action="" method="post" id="search">-->
 username id: <input type="text" name="usernameID" id="uid">
 <!--  <input type="hidden" name="status" value="query">-->
 <button type="button" onclick="search()">search</button>
 <!--  </form>-->
 
<table class="table">
	<tr>
		<td scope="col">no&nbsp;</td>
		<td scope="col">name&nbsp;</td>
		<td scope="col">id</td>
		<td scope="col">gender&nbsp;</td>
		<td scope="col">role&nbsp;</td>
		<td>actions</td>
	</tr>
		<c:forEach items="${userList}" var="i">
		<tr>
			<td scope="row">${i.userId}</td>
			<td>${i.username}</td>
			<td>${i.usernameID}</td>
			<td>${i.gender==1?'male':'female'}</td>
			<td>${i.roleName}</td>
			<td>
				<button type='button' onclick="editUser('${i.userId}','${i.username}','${i.usernameID}','${i.gender}','${i.roleId}','${i.userPassword}')">edit</button>
				<button type='button' onclick="delUser(${i.userId})">delete</button>
			</td>
		</tr>
		</c:forEach>
</table>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
  	<script>
  		function addUser(){
  			window.location.href="<%=path%>/pages/user/addUser.jsp";
  		}
  		
  		function editUser(no,name,id,gender,roleId,pass){
  			//console.log('edit');
  			window.location.href="<%=path%>/pages/user/update.jsp?no="+no+"&username="+name+"&id="+id+"&password="+pass+"&gender="+gender+"&role="+roleId;
  		}
  		
  		function delUser(userId){
  			window.location.href="<%=path%>/userServlet?no="+userId+"&status=delete";
  		}
  		
  		function logout(){
  			window.location.href="<%=path%>/login.jsp";
  		}
  		
  		function search(){
  			var uid=document.getElementById('uid').value;
  			console.log(uid);
  			if (uid==""||uid.length === 0){
  				cancel();
  				return;
  			}
  			
  			window.location.href="<%=path%>/userServlet?usernameID="+uid+"&status=query";
  		}
  		
  		function cancel(){
  			window.location.href="<%=path%>/userServlet?status=login";
  		}
  	</script>
  </body>
</html>