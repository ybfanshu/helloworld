<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<% String path=request.getContextPath(); %>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
  </head>
  <body>
  <!-- pages/user/addUser.jsp -->
	<form action="<%=path%>/userServlet" method="POST" id="form">
		name: <input type="text" id="username" name="username"><br>
		user id: <input type="text" id="usernameID" name="usernameID"><br>
		password: <input type="text" id="userPassword" name="userPassword"><br>
		gender: <input type="radio" id="gender1" name="gender" value="1" checked="checked">male <input type="radio" id="gender2" name="gender" value="2">female<br>
		role: <select id="roleId" name="roleId">
			<option value="">select</option>
			<option value="1">管理員</option>
			<option value="2">會員</option>
		</select>
		<input type="hidden" name="status" value="add" />
		<br><br>
		<button type="button" onclick="add()">add</button>
		<button type="button" onclick="cancel()">cancel</button>
	</form>
	
	<script>
	function add(){
		var username=document.getElementById("username").value;
		var usernameID=document.getElementById("usernameID").value;
		var userPassword=document.getElementById("userPassword").value;
		var roleId=document.getElementById("roleId").value;
		
		if(username==null||username==''){
			alert('missing username');
			return;
		}
		if(usernameID==null||usernameID==''){
			alert('missing id');
			return;
		}
		if(userPassword==null||userPassword==''){
			alert('missing password');
			return;
		}
		if(roleId==null||roleId==''){
			alert('missing role');
			return;
		}
		
		var url="<%=path%>/userServlet";
		var params='username='+username;
		//var ret=getDataByAjax(url,params);
		<% session.setAttribute("addnew","yes"); %>
		document.getElementById("form").submit();
		
	}
	function cancel(){
		window.location.href="<%=path%>/userServlet?status=login";
	}
	
	function getDataByAjax(url,params){
		var ajaxObj=null;
		ajaxObj.open('POST',url,false);
		ajaxObj.send(params);
		return ajaxObj.responseText;
	}
	</script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
  </body>
</html>