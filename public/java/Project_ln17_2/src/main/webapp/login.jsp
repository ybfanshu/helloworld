<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
  </head>
  <body>
  
	<form action="loginServlet" method="POST" id="form" style="padding-left:100px;">
		<div class="row g-3 align-items-center">
		<div class="col-auto">
		id: <input class="form-control" type="text" name="usernameID" id="username"><br>
		pass: <input class="form-control" type="password" name="userPassword" id="userpass"><br>
		<input type="hidden" name="status" value="login" />
		<button class="mx-auto" style="width: 200px;" type="button" name="btnB" onclick="loginVerify()">login</button>
		</div></div>
	</form>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
    <script>
    	function loginVerify(){
    		username=document.getElementById("username");
    		password=document.getElementById("userpass");
    		
		if(username=='' || password=='' ){
			alert('blank input');
			return;
		}

    		document.getElementById("form").submit();
    	}    
    </script>
  </body>
</html>