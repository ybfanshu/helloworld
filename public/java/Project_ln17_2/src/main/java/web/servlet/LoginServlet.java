package web.servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import web.service.UserService;
import web.service.impl.UserServiceImpl;
import web.vo.User;

import java.io.IOException;
import java.sql.SQLException;

public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// /loginServlet
		String username = request.getParameter("usernameID");
		String password = request.getParameter("userPassword");

		try {
			UserService userservice = new UserServiceImpl();
			User user = userservice.queryUserByNameId(username);
			System.out.println(user);
			if (user != null && user.getUserPassword().equals(password)) {

				// if("admin".equals(username) && "123".equals(password)) {
				request.setAttribute("username", username);
				request.getRequestDispatcher("userServlet").forward(request, response);
			} else {
				request.setAttribute("error", "account info error");
				request.getRequestDispatcher("login.jsp").forward(request, response);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
