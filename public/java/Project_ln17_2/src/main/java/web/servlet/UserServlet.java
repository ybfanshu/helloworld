package web.servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import web.dao.UserDao;
import web.dao.impl.UserDaoImpl;
import web.service.UserService;
import web.service.impl.UserServiceImpl;
import web.vo.User;

import java.io.IOException;
import java.util.List;

public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// /userServlet
		UserService userService = new UserServiceImpl();
		List<User> list = userService.queryUsers();

		UserDao userDao = new UserDaoImpl();
		String status = request.getParameter("status");
		if (status.equals("add")) {
			System.out.println("add");
			HttpSession session = request.getSession();
			String addnew = (String) session.getAttribute("addnew");
			if (addnew.equals("yes")) {
				User user = new User();
				user.setUsername(request.getParameter("username"));
				user.setUsernameID(request.getParameter("usernameID"));
				user.setUserPassword(request.getParameter("userPassword"));
				user.setGender(Integer.parseInt(request.getParameter("gender")));
				user.setRoleId(Integer.parseInt(request.getParameter("roleId")));
				userDao.add(user);
				session.setAttribute("addnew", "no");

				response.sendRedirect("userServlet?status=login");
			}
		} else if (status.equals("delete")) {
			int no = Integer.parseInt(request.getParameter("no"));
			// System.out.println(no);
			userDao.delete(no);
			// request.setAttribute("userList", list);
			response.sendRedirect("userServlet?status=login");
		} else if (status.equals("update")) { //edit user info
			HttpSession session = request.getSession();
			String toEdit = (String) session.getAttribute("toEdit");
			System.out.println("edit");
			if (toEdit.equals("yes")) {
				User user = new User();
				user.setUserId(Integer.parseInt(request.getParameter("userId")));
				user.setUsername(request.getParameter("username"));
				user.setUsernameID(request.getParameter("usernameID"));
				user.setUserPassword(request.getParameter("userPassword"));
				//user.setGender(Integer.parseInt(request.getParameter("gender")));
				//user.setRoleId(Integer.parseInt(request.getParameter("roleId")));
				userDao.update(user);
				session.setAttribute("toEdit", "no");
				response.sendRedirect("userServlet?status=login");
			}
		} else if (status.equals("query")) {
			System.out.println("query");
			String usernameID=request.getParameter("usernameID");
			List<User> users=userDao.search(usernameID);
			
			//request.setAttribute("usernameID", usernameID);
			request.setAttribute("userList", users);
			request.getRequestDispatcher("/pages/user/userlist.jsp").forward(request, response);
		} else if (status.equals("login") || status.equals("")) {
			// System.out.println("login");
			request.setAttribute("userList", list);
			request.getRequestDispatcher("/pages/user/userlist.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
