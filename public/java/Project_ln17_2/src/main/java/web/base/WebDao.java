package web.base;

import java.util.List;

public interface WebDao<T> {
	public void add(T t);

	 //刪除
	 public void delete(int userId);

	 //更新
	 public void update(T t);

	 //搜尋全部
	 public List<T> search(String usernameID);
}
