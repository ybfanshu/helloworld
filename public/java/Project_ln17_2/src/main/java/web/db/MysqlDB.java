package web.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class MysqlDB {
	private final  String JDBC_DRIVER="com.mysql.cj.jdbc.Driver";
	private final  String JDBC_URL="jdbc:mysql:///project";
	private final  String JDBC_USERNAME="root";
	private final  String JDBC_USERPASS="Password123";
	private final Connection conn=null;
	
	private MysqlDB() {
		try {
			//STEP 2: Register JDBC driver
			Class.forName(JDBC_DRIVER);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static MysqlDB getDbUtil() {
		return new MysqlDB();
	}
	
	public Connection getConnection() {
		try {
			//STEP 3: Open a connection
			return DriverManager.getConnection(JDBC_URL, JDBC_USERNAME, JDBC_USERPASS);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public void close(Connection conn) {
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void close(PreparedStatement ps) {
		if (ps != null) {
			try {
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void close(Statement st) {
		if (st != null) {
			try {
				st.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void close(ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
