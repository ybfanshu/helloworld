package web.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import web.dao.UserDao;
import web.db.MysqlDB;
import web.vo.User;

public class UserDaoImpl extends UserDao {
	Connection conn = null;
	PreparedStatement ps = null;

	@Override
	public void add(User user) {
		try {
			conn = MysqlDB.getDbUtil().getConnection();
			String addSQL = "insert into userinfo(username,usernameID,gender,roleId,userPassword)values(?,?,?,?,?);";

			ps = conn.prepareStatement(addSQL);
			ps.setString(1, user.getUsername());
			ps.setString(2, user.getUsernameID());
			ps.setInt(3, user.getGender());
			ps.setInt(4, user.getRoleId());
			ps.setString(5, user.getUserPassword());
			ps.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			MysqlDB.getDbUtil().close(ps);
			MysqlDB.getDbUtil().close(conn);
		}
	}

	@Override
	public void delete(int userId) {
		Connection conn = MysqlDB.getDbUtil().getConnection();
		PreparedStatement ps = null;
		String deleteSQL = "delete from userinfo where userId=?";
		try {
			ps = conn.prepareStatement(deleteSQL);
			ps.setInt(1, userId);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void update(User user) {
		Connection conn = MysqlDB.getDbUtil().getConnection();
		PreparedStatement ps = null;
		String updateSql = "update userinfo set username=?, usernameID=?, userPassword=? where userId=?;";

		try {
			ps = conn.prepareStatement(updateSql);
			ps.setString(1, user.getUsername());
			ps.setString(2, user.getUsernameID());
			ps.setString(3, user.getUserPassword());
			// ps.setInt(4, user.getRoleId());
			ps.setInt(4, user.getUserId());
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<User> search(String usernameID) {
		List<User> list = new ArrayList<User>();
		Connection conn = MysqlDB.getDbUtil().getConnection();
		//String querySql="select * from userinfo where userId like ?";
		String querySql="select u.*,r.roleName from userinfo u left join roleinfo r on r.roleId=u.roleId where u.usernameID=?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			ps=conn.prepareStatement(querySql);
			ps.setString(1,usernameID);
			rs=ps.executeQuery();
			while (rs.next()) {
				User user = new User();
				user.setUserId(rs.getInt("userId"));
				user.setUsername(rs.getString("username"));
				user.setUsernameID(rs.getString("usernameID"));
				user.setGender(rs.getInt("gender"));
				user.setUserPassword(rs.getString("userPassword"));
				user.setRoleId(rs.getInt("roleId"));
				user.setRoleName(rs.getString("roleName"));
				list.add(user);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
		//return null;
	}


	
	//	older
	@Override
	public List<User> queryUsers() {
		Connection conn = MysqlDB.getDbUtil().getConnection();
		String querySql = "select u.*,r.roleName from userinfo u left join roleinfo r on r.roleId=u.roleId;";
		List<User> list = new ArrayList<>();
		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(querySql);
			while (rs.next()) {
				User user = new User();
				user.setUserId(rs.getInt("userId"));
				user.setUsername(rs.getString("username"));
				user.setUsernameID(rs.getString("usernameID"));
				user.setGender(rs.getInt("gender"));
				user.setUserPassword(rs.getString("userPassword"));
				user.setRoleId(rs.getInt("roleId"));
				user.setRoleName(rs.getString("roleName"));
				list.add(user);
			}
		} catch (SQLException e) {

			e.printStackTrace();
		} finally {
			MysqlDB.getDbUtil().close(rs);
			MysqlDB.getDbUtil().close(stmt);
			MysqlDB.getDbUtil().close(conn);
		}
		return list;
	}

	@Override
	public User queryUserByNameId(String usernameID) throws SQLException {
		Connection conn = MysqlDB.getDbUtil().getConnection();
		// String querySql="select u.* form userinfo u where
		// u.usernameID='"+usernameID+"';";
		String querySql="select u.*,r.roleName from userinfo u left join roleinfo r on r.roleId=u.roleId where u.usernameID='"+usernameID+"';";		
		// List<User> list=new ArrayList<>();
		Statement stmt = null;
		ResultSet rs = null;
		User user = null;
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(querySql);

			while (rs.next()) {
				user = new User();
				user.setUserId(rs.getInt("userId"));
				user.setUsername(rs.getString("username"));
				user.setUsernameID(rs.getString("usernameID"));
				user.setGender(rs.getInt("gender"));
				user.setUserPassword(rs.getString("userPassword"));
				user.setRoleId(rs.getInt("roleId"));
				user.setRoleName(rs.getString("roleName"));
				// list.add(user);
			}
		} catch (SQLException e) {

			e.printStackTrace();
		} finally {
			MysqlDB.getDbUtil().close(rs);
			MysqlDB.getDbUtil().close(stmt);
			MysqlDB.getDbUtil().close(conn);
		}
		return user;
	}

}
