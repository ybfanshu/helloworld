package web.dao;

import java.sql.SQLException;
import java.util.List;

import web.base.WebDao;
import web.vo.User;

public abstract class UserDao implements WebDao<User>{


	//search all
	public abstract List<User> queryUsers();
	
	//search individual
	public abstract User queryUserByNameId(String usernameID) throws SQLException;

}
