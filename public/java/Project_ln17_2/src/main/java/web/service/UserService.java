package web.service;

import java.sql.SQLException;
import java.util.List;

import web.vo.User;


public interface UserService {
	List<User> queryUsers();

	User queryUserByNameId(String usernameID) throws SQLException;

}
