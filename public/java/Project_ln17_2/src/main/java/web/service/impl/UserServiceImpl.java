package web.service.impl;

import java.sql.SQLException;
import java.util.List;

import web.dao.UserDao;
import web.dao.impl.UserDaoImpl;
import web.service.UserService;
import web.vo.User;

public class UserServiceImpl implements UserService {

	@Override
	public List<User> queryUsers() {
		UserDao userDao=new UserDaoImpl();
		return userDao.queryUsers();
		//return null;
	}

	@Override
	public User queryUserByNameId(String usernameID) throws SQLException {
		UserDao userDao=new UserDaoImpl();
		return userDao.queryUserByNameId(usernameID);
	}

}
