package mavenn.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import mavenn.entity.User;

public interface UserRepo extends JpaRepository<User, Integer> {

}
