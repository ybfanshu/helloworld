package mavenn.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import mavenn.entity.User;
import mavenn.repos.UserRepo;

//localhost:8080/api/insert
//api/findAll
//api/getuser
@RestController
@RequestMapping("/api")
public class UserController {
	@Autowired
	private UserRepo userRepo;
	
	@RequestMapping("/getuser")
	public ModelAndView getuserPage() {
		// escape restcontroller
		return new ModelAndView("getUser");
	}
	
	@PostMapping("/insert")
	public void insertUser(@RequestBody User user) {
		userRepo.save(user);
	}
	
	@GetMapping("/findAll")
	public List<User> getUser(){
		return userRepo.findAll();
	}
}
