package mavenn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootAndJpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootAndJpaApplication.class, args);
	}

}
